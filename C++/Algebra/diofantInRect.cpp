li gcd (li a, li b, li& x, li& y)
{
	if (b == 0)
	{
		x = 1, y = 0;
		return a;
	}

	li x1, y1;
	li g = gcd(b, a % b, x1, y1);

	x = y1;
	y = x1 - y1 * (a / b);

	return g;
}

inline li up (li a, li b)
{
	if (a < 0)
	{
		a = -a;
		b = -b;
	}

	if (b > 0)
		return (a + b - 1) / b;

	return a / b;
}

inline li down (li a, li b)
{
	if (a < 0)
	{
		a = -a;
		b = -b;
	}

	if (b > 0)
		return a / b;

	return (a - b - 1) / b;
}

//a1<=x<=a2 && b1<=y<=b2
//(x1,y1)!=(x2,y2)
inline li getCnt (li a1, li a2, li b1, li b2, li x1, li y1, li x2, li y2)
{
	li A = y1 - y2, B = x2 - x1;
	li C = x2 * 1ll * y1 - y2 * 1ll * x1;

	li x, y;
	li g = gcd(abs(A), abs(B), x, y);

	if (abs(C) % g != 0)
		return 0;

	x *= C / g;
	y *= C / g;

	if (A < 0)
		x = -x;
	if (B < 0)
		y = -y;

	A /= g;
	B /= g;

	li l = -INF64, r = INF64;
	if (B > 0)
	{
		l = max(l, up(a1 - x, B));
		r = min(r, down(a2 - x, B));
	}
	if (B < 0)
	{
		l = max(l, up(a2 - x, B));
		r = min(r, down(a1 - x, B));
	}
	if (B == 0)
	{
		if (x < a1 || x > a2)
			return 0;
	}
	if (A > 0)
	{
		l = max(l, up(y - b2, A));
		r = min(r, down(y - b1, A));
	}
	if (A < 0)
	{
		l = max(l, up(y - b1, A));
		r = min(r, down(y - b2, A));
	}
	if (A == 0)
	{
		if (y < b1 || y > b2)
			return 0;
	}

	li ans = max(li(0), r - l + 1);

	return ans;
}
