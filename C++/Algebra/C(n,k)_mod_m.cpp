//C(n,k) % m, m >= 2, n >= 1, 0 <= k <= n
//O(m * countDivs(m) * logn) ~ O(mlog2)

inline int binPow (int a, li b, int mod)
{
	int ans = 1;
	while (b)
	{
		if (b & 1ll)
			ans = int((ans * 1ll * a) % mod);

		b >>= 1ll;
		a = int((a * 1ll * a) % mod);
	}

	return ans;
}

const int N = 100 + 13; //count of prime dividers of m

int szp;
pt p[N];

inline void getDivs (int n)
{
	szp = 0;

	for (int i = 2; i * i <= n; i++)
	{
		int cnt = 0;
		while (n % i == 0)
		{
			n /= i;
			cnt++;
		}

		if (cnt)
			p[szp++] = mp(i, cnt);
	}

	if (n > 1)
		p[szp++] = mp(n, 1);
}

int a[N];

inline bool check (int val)
{
	forn (i, szp)
	{
		int mod = 1;
		forn (j, p[i].sc)
			mod *= p[i].ft;

		if (val % mod != a[i])
			return false;
	}

	return true;
}

const int M = 1000 * 1000 + 13;
int fp[M];

inline void precalcFp(int p, int mod)
{
	fp[0] = 1;
	for (int i = 1; i <= mod; i++)
	{
		if (i % p == 0)
		{
			fp[i] = fp[i - 1];
			continue;
		}

		fp[i] = int((fp[i - 1] * 1ll * i) % mod);
	}
}

inline pair<li, int> calcF (li n, int p, int mod)
{
	li ans1 = 0;

	li nn = n / p;
	while (nn)
	{
		ans1 += nn;
		nn /= p;
	}

	int ans2 = 1;

	nn = n;
	while (nn)
	{
		int cur = int((binPow(fp[mod - 1], nn / mod, mod) * 1ll * fp[nn % mod]) % mod);

		ans2 = int((ans2 * 1ll * cur) % mod);

		nn /= p;
	}

	return mp(ans1, ans2);
}

inline int calcC(li n, li k, int m)
{
	getDivs(m);

	forn (i, szp)
	{
		int mod = 1;
		forn (j, p[i].sc)
			mod *= p[i].ft;

		precalcFp(p[i].ft, mod);

		pair<li, int> fn = calcF(n, p[i].ft, mod);
		pair<li, int> fk = calcF(k, p[i].ft, mod);
		pair<li, int> fnk = calcF(n - k, p[i].ft, mod);

		int fi = mod - (mod / p[i].ft);

		pair<li, int> ans;
		ans.ft = fn.ft - fk.ft - fnk.ft;
		ans.sc = int((fn.sc * 1ll * binPow(fk.sc, fi - 1, mod)) % mod);
		ans.sc = int((ans.sc * 1ll * binPow(fnk.sc, fi - 1, mod)) % mod);

		if (ans.ft >= p[i].sc)
			a[i] = 0;
		else
		{
			forn (j, ans.ft)
				ans.sc = int((ans.sc * 1ll * p[j].ft) % mod);

			a[i] = ans.sc;
		}
	}

	forn (i, m)
		if (check(i))
			return i;

	throw;
}