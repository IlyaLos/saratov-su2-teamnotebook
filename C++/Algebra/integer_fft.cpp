//int, 998244353 2^23 g = 3
//int, 1004535809, 2^19 g = 3
//int, 469762049, 2^26 g = 3
//li, 10000093151233, 2^26 g = 5
//li, 1000000523862017, 2^26 g = 3
//li, 1000000000949747713, 2^26 g = 2

const int N = 2200 * 1000 + 13;
const li MOD = 1000000523862017ll
li g = 3;

inline li mult (li a, li b)
{
	li q = li(ld(a) * b / MOD);
	li ans = (a * b - q * MOD) % MOD;
	
	if (ans < 0)
		ans += MOD;
		
	return ans;
}

inline li norm (li val)
{
	if (val >= MOD)
		val -= MOD;
		
	if (val < 0)
		val += MOD;
		
	return val;
}

inline li binPow (li a, li b)
{
	li ans = 1;
	while (b)
	{
		if (b & 1)
			ans = mult(ans, a);
			
		a = mult(a, a);
		b >>= 1;
	}
	
	return ans;
}

inline li rev (li a)
{
	return binPow(a, MOD - 2);
}

inline void fft (li a[N], int n, int logn, bool invert) 
{
	for (int i=1, j=0; i<n; ++i) 
	{
		int bit = n >> 1;
		for (; j>=bit; bit>>=1)
			j -= bit;
		j += bit;
		if (i < j)
			swap(a[i], a[j]);
	}
 
	for (int i = 0; i < logn; i++)
	{
		li cg = binPow(g, 1 << (logn - (i + 1)));
		if (invert)
			cg = rev(cg);
			
		int len = 1 << (i + 1);
			
		for (int i = 0; i < n; i += len) 
		{
			li cur = 1;
			for (int j = 0; j < len / 2; j++) 
			{
				li u = a[i + j],  v = mult(a[i + j + len / 2], cur);
				a[i + j] = norm(u + v);
				a[i + j + len / 2] = norm(u - v);
				cur = mult(cur, cg);
			}
		}
	}
	if (invert) {
		li nrev = rev(n);
		for (int i=0; i<n; ++i)
			a[i] = mult(a[i], nrev);
	}
}

li ans[N];

inline void multiply(li a[N], li b[N])
{
	int n = (1 << 21);
	g = binPow(g, (MOD - 1) >> 21);

	fft(a, n, 21, 0);
	fft(b, n, 21, 0);
	
	forn (i, n)
		ans[i] = mult(a[i], b[i]);

	fft(ans, n, 21, 1);
}
