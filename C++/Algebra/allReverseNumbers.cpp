inline int norm (int val, int mod)
{
	if (val >= mod)
		val -= mod;

	if (val < 0)
		val += mod;

	return val;
}

int inv[N];

inline void findAllRev(int n) //n is prime!
{
	inv[1] = 1;
	for (int i = 2; i < n; i++)
		inv[i] = norm(int((-(n / i) * 1ll * inv[n % i]) % n), n);
}