const int N = 800 + 13;
int n;
int a[N][N];

forn (i, n)
{
	int v = i;
    
	for (int j = i + 1; j < n; j++)
		if (abs(a[j][i]) > abs(a[v][i])) 
			v = j;
			
	forn (j, n + 1)
		swap(a[i][j], a[v][j]);
    
	forn (j, n)
		if (i != j)
			for (int k = i + 1; k <= n; k++)
       			a[j][k] -= a[i][k] * (a[j][i] / a[i][i]);
}

forn (i, n)
	ans[i] = a[i][n] / a[i][i];
