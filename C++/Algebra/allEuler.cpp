//Computing Euler's totient function for all natural numbers <= n in O(n*log n):

int phi[N];

for (int i = 1; i <= n; i++) 
{
  phi[i] += i;

  for (int j = 2 * i; j <= n; j += i) 
    phi[j] -= phi[i];
}
