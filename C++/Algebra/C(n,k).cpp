inline li C (int n, int k) 
{
	ld res = 1;
	for (int i = 1; i <= k; ++i)
		res = res * (n - k + i) / i;
	
    return (li) (res + 0.01);
}