//можно ld на double заменить ускорение еще в 2 раза
struct comp {
	ld r, i; 
	comp():r(0), i(0){}
	comp(int r):r(r), i(0){}
	comp(ld r, ld i):r(r), i(i){}
	ld real() { return r; }
};

inline comp operator+(const comp& a, const comp& b) { return comp(a.r + b.r, a.i + b.i); }
inline comp operator-(const comp& a, const comp& b) { return comp(a.r- b.r, a.i - b.i); }
inline comp operator*(const comp& a, const comp& b) { return comp(a.r * b.r - a.i * b.i, a.i * b.r + b.i * a.r); }
inline comp operator/(const comp& a, const ld& b) { return comp(a.r / b, a.i / b); }

comp aa[N], bb[N];
int rev_calced = -1;
int rev[N];
comp w[LOGN][N];

void fft(comp a[N], int n, bool invert = false) {
	forn(i, n)
		if (i < rev[i])
			swap(a[i], a[ rev[i] ]);

	for(int len = 2, k = 1; len <= n; len <<= 1, k++) {
		int len2 = (len >> 1);
		
		for(int i = 0; i < n; i += len) {
			comp t, *pu = a + i, *pv = a + i + len2, *pu_end = a + i + len2, *pw = w[k - 1];
			for(; pu != pu_end; pu++, pv++, pw++) {
				if (invert)
					t = *pv * comp(pw->r, -pw->i);
				else
					t = *pv * *pw;
					
				*pv = *pu - t;
				*pu = *pu + t;
			}
		}
	}
	if (invert)
		forn(i, n) 
			a[i] = a[i] / n;
}

void precalc(int n) {
	if (n == rev_calced)
		return;
	rev_calced = n;
	for(int i = 1, j = 0;  i < n; i++) {
		int bit = n >> 1;
		for (; j >= bit; bit >>= 1)
			j -= bit;
		j += bit;
		rev[i] = j;
	}
	for (int len = 2, k = 1; len <= n; len <<= 1, k++)
	{
		int len2 = (len >> 1);
		ld ang = 2 * PI / len;
		
		forn(i, len2)
			w[k - 1][i] = comp(cos(ang * i), sin(ang * i));		
	}
}

void multiply(int a[N], int b[N], int c[N], int sza, int szb, int& n) {
	n = 1;
	while (n < sza + szb) // иногда сумму можно заменить на максимум
		n <<= 1;
	
	assert(n < N);
	
	precalc(n);
	forn(i, n) aa[i] = comp(a[i]);
	forn(i, n) bb[i] = comp(b[i]);

	fft(aa, n);
	fft(bb, n);
	
	forn(i, n)
		aa[i] = aa[i] * bb[i];

	fft(aa, n, true);
	
	forn(i, n) {
		c[i] = int(aa[i].real() + .5);
	}	
}
