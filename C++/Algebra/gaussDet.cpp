const int N = 800 + 13;
int n;
ld a[N][N];

ld det = 1;
forn (i, n)
{
	int k = i;
	
	for (int j = i + 1; j < n; j++)
		if (abs(a[j][i]) > abs(a[k][i]))
			k = j;
			
	if (abs(a[k][i]) < EPS) 
	{
		det = 0;
		break;
	}
	
	swap(a[i], a[k]);
	
	if (i != k)
		det = -det;
		
	det *= a[i][i];
	
	for (int j = i + 1; j < n; j++)
		a[i][j] /= a[i][i];
		
	forn (j, n)
		if (j != i && abs(a[j][i]) > EPS)
			for (int k = i + 1; k < n; k++)
				a[j][k] -= a[i][k] * a[j][i];
}
