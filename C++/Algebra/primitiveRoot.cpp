//Если есть хотя бы 1, то всего их fi(fi(n))
//Существует для n = p^k (p - простое нечётное), n = p ^ (2 * k) (p - простое), n = 1, n = 2, n = 4

int divs[1000];
int szdivs = 0;

inline int phi (int n)
{
	szdivs = 0;
	
	int ans = n;
	
	for (int i = 2; i * i <= n; i++)
	{
		if (n % i != 0)
			continue;
			
		divs[szdivs++] = i;
			
		ans -= ans / i;
		
		while (n % i == 0)
			n /= i;
	}
	
	if (n > 1)
	{
		divs[szdivs++] = n;
		ans -= ans / n;
	}
		
	return ans;
}

inline int pow (int a, int b, int n)
{
	int ans = 1;
	while (b)
	{
		if (b & 1)
			ans = (ans * 1ll * a) % n;
			
		a = (a * 1ll * a) % n;
		b >>= 1;
	}
	
	return ans;
}

inline int getGenerator (int n)
{
	int phin = phi(n);
	int pphi = phi(phin);
	
	for (int i = 2; i < n; i++)
	{
		if (__gcd(i, n) != 1)
			continue;
			
		bool ok = 1;

		forn (j, szdivs)
			if (pow(i, phin / divs[j], n) == 1)
			{
				ok = 0;
				break;
			}
			
		if (ok)
			return i;
	}
	
	return -1;
}
