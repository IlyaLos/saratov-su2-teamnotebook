ld a, b;

inline ld integrate()
{
    int N = 1000 * 1000;

    ld ans = 0;
    ld h = (b - a) / ld(N);

    forn (i, N + 1)
    {
    	ld x = a + h * i;

    	ans += f(x) * ((i == 0 || i == N) ? 1 : ((!(i & 1)) ? 2 : 4));
    }

    ans *= h / ld(3);

    return ans;
}
