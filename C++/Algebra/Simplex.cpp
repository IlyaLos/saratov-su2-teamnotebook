#include <bits/stdc++.h>

using namespace std;

#define maxm 20002
#define maxn 202
double INF = 1e100;
double eps = 1e-10;
double pi = acosl(-1);

int counter = 0;
void pivot(int m, int n, double a[maxm][maxn], int B[maxm], int N[maxn], int r, int c) {
    int i, j;
    swap(N[c], B[r]);
    a[r][c]=1/a[r][c];
    for (j=0; j<=n; j++)if (j!=c) a[r][j]*=a[r][c];
    for (i=0; i<=m; i++)if (i!=r) {
            for (j=0; j<=n; j++)if (j!=c)
                    a[i][j]-=a[i][c]*a[r][j];
            a[i][c] = -a[i][c]*a[r][c];
        }
       counter++;
}

int feasible(int m, int n, double a[maxm][maxn], int B[maxm], int N[maxn]) {
    int r = 0, c = 0, i; double p, v;
    while (1) {
        for (p=INF, i=0; i<m; i++) if (a[i][n]<p) p=a[r=i][n];
        if (p>-eps) return 1;
        for (p=0, i=0; i<n; i++) if (a[r][i]<p) p=a[r][c=i];
        if (p>-eps) return 0;
        p = a[r][n]/a[r][c];
        for (i=r+1; i<m; i++) if (a[i][c]>eps) {
                v = a[i][n]/a[i][c];
                if (v<p) r=i, p=v;
            }
        pivot(m, n, a, B, N, r, c);
    }
}

int simplex(int m, int n, double a[maxm][maxn], double b[maxn], double& ret) {
    int B[maxm], N[maxn], r = 0, c = 0, i; double p, v;
    for (i=0; i<n; i++) N[i]=i;
    for (i=0; i<m; i++) B[i]=n+i;
    if (!feasible(m, n, a, B, N)) return 0;
    while (1) {
        for (p=0, i=0; i<n; i++) if (a[m][i]>p)
                p=a[m][c=i];
        if (p<eps) {
            for (i=0; i<n; i++) if (N[i]<n)
                    b[N[i]]=0;
            for (i=0; i<m; i++) if (B[i]<n)
                    b[B[i]]=a[i][n];
            ret = -a[m][n];
            return 1;
        }
        for (p=INF, i=0; i<m; i++) if (a[i][c]>eps) {
                v = a[i][n]/a[i][c];
                if (v<p) p=v, r=i;
            }
        if (p==INF) return -1;
        pivot(m, n, a, B, N, r, c);
    }
}

typedef pair<int,int> pii;

double distance(pii &a, pii &b) {
    double ret = (a.first - b.first) * (a.first - b.first);
    ret += (a.second - b.second) * (a.second - b.second);
    return sqrt(ret);
}

double a[maxm][maxn], b[maxn];

int main() {

    return 0;
}
