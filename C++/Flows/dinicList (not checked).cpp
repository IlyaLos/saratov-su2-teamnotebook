const int N = 1000 + 13;  //count of vertexs
const int M = 100 * 1000 + 13; //count of edges

struct edge
{
	int to;
	int cap;
	int flow;

	edge() {}
	edge(int to, int cap) : to(to), cap(cap), flow(0) {}
};

int sze = 0;
edge e[M];

vector<int> g[N];

inline void addEdge(int v, int to, int cap)
{
 	g[v].pb(sze);
 	e[sze++] = edge(to, cap);

 	g[to].pb(sze);
 	e[sze++] = edge(v, 0);
}

int d[N];
int ptr[N], q[M];
int s, t;

inline bool bfs()
{
	int head = 0, tail = 0;
	q[tail++] = s;

	forn (i, n + 2)     //be careful
		d[i] = -1;

	d[s] = 0;

	while (head != tail && d[t] == -1)
	{
		int v = q[head++];
		if (head == M)
			head = 0;

		forn(i, sz(g[v]))
		{
			int id = g[v][i];
		    int to = e[id].to;

			if (d[to] == -1 && e[id].flow < e[id].cap)
			{
				q[tail++] = to;
				if (tail == M)
					tail = 0;

				d[to] = d[v] + 1;
			}
		}
	}

	return d[t] != -1;
}

int dfs (int v, int flow)
{
	if (!flow)
	    return 0;

	if (v == t)
	    return flow;

	for (; ptr[v] < sz(g[v]); ++ptr[v])
	{
		int id = g[v][ptr[v]];
	    int to = e[id].to;

		if (d[to] != d[v] + 1)
		    continue;

		int pushed = dfs(to, min(flow, e[id].cap - e[id].flow));

		if (pushed)
		{
			e[id].flow += pushed;
			e[id ^ 1].flow -= pushed;

			return pushed;
		}
	}

	return 0;
}

inline int dinic()
{
	int flow = 0;

	while(true)
	{
		if (!bfs())
		    break;

		forn (i, n + 2)  //be careful
			ptr[i] = 0;

		int pushed = dfs(s, INF);
		while (pushed)
		{
			flow += pushed;
			pushed = dfs(s, INF);
		}
	}

	return flow;
}
