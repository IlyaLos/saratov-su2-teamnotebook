const int K = 100 * 1000 + 3; //count of vertex
const int M = 100 * 1000 + 13; //count of edges

struct edge
{
	int to;
	int cap;
	int cost;
	int flow;

	edge() {}
	edge(int to, int cap, int cost) : to(to), cap(cap), cost(cost), flow(0) {}
};

int sze = 0;
edge e[M];

vector<int> g[K];

inline void addEdge(int v, int to, int cap, int cost)
{
 	g[v].pb(sze);
 	e[sze++] = edge(to, cap, cost);

 	g[to].pb(sze);
 	e[sze++] = edge(v, 0, -cost);
}

int q[M], head, tail;
char inQ[K];
int d[K];
int p[K], pe[K];

int flow, cost;

inline bool enlarge(int s, int t)
{
	head = tail = 0;
		
	forn (i, t + 1)  //be careful!!
	{
		d[i] = INF;
		inQ[i] = 0;
	}
	
	d[s] = 0;
	inQ[s] = 1;
	q[tail++] = s;
	
	while (head != tail)
	{
		int v = q[head++];
		inQ[v] = 0;
		
		if (head == M)
			head = 0;
			
		forn (i, sz(g[v]))
		{
			int id = g[v][i];
			
			if (e[id].flow >= e[id].cap)
				continue;
				
			int to = e[id].to;
			
			if (d[to] > d[v] + e[id].cost)
			{
				d[to] = d[v] + e[id].cost;
				p[to] = v;
				pe[to] = id;
				
				if (!inQ[to])
				{
					inQ[to] = 1;
					q[tail++] = to;
					
					if (tail == M)
						tail = 0;
				}
			}
		}
	}
	
	if (d[t] == INF)
		return false;
		
	int addFlow = INF;
	
	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];
		
		addFlow = min(addFlow, e[id].cap - e[id].flow);
	}
	
	flow += addFlow;
	
	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];
		
		e[id].flow += addFlow;
		e[id ^ 1].flow -= addFlow;
	}
	
	cost += d[t] * addFlow;
	
	return true;
}
