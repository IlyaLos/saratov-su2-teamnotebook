//в итоге получается дерево ng. каждая компонента i состоит только из одной вершины id[ comp[i] ].
//minCut между s и t равен минимуму на пути между компонентами вершин s и t.

const int N = 4500 + 13;
int n, m;
pair<pt, int> gg[N]; //list of edges

const int K = 100 * 1000 + 3; //count of vertex
const int M = 100 * 1000 + 13; //count of edges

struct edge
{
	int to;
	int cap;
	int flow;

	edge() {}
	edge(int to, int cap) : to(to), cap(cap), flow(0) {}
};

int sze = 0;
edge e[M];

vector<int> g[K];

inline void addEdge(int v, int to, int cap)
{
 	g[v].pb(sze);
 	e[sze++] = edge(to, cap);

 	g[to].pb(sze);
 	e[sze++] = edge(v, cap);
}

int q[M], head, tail;
char inQ[K];
int d[K];
int p[K], pe[K];

int flow;

inline bool enlarge(int s, int t)
{
	head = tail = 0;
		
	forn (i, n)
	{
		d[i] = INF;
		inQ[i] = 0;
	}
	
	d[s] = 0;
	inQ[s] = 1;
	q[tail++] = s;
	
	while (head != tail)
	{
		int v = q[head++];
		inQ[v] = 0;
		
		if (head == M)
			head = 0;
			
		forn (i, sz(g[v]))
		{
			int id = g[v][i];
			
			if (e[id].flow >= e[id].cap)
				continue;
				
			int to = e[id].to;
			
			if (d[to] > d[v] + 1)
			{
				d[to] = d[v] + 1;
				p[to] = v;
				pe[to] = id;
				
				if (!inQ[to])
				{
					inQ[to] = 1;
					q[tail++] = to;
					
					if (tail == M)
						tail = 0;
				}
			}
		}
	}
	
	if (d[t] == INF)
		return false;
		
	int addFlow = INF;
	
	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];
		
		addFlow = min(addFlow, e[id].cap - e[id].flow);
	}
	
	flow += addFlow;
	
	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];
		
		e[id].flow += addFlow;
		e[id ^ 1].flow -= addFlow;
	}
	
	return true;
}

inline void minCut (int s, int t)
{
	flow = 0;
	sze = 0;
	forn (i, n)
		g[i].clear();
		
	forn (i, m)
		addEdge(gg[i].ft.ft, gg[i].ft.sc, gg[i].sc);
		
	while (enlarge(s, t))
		;
}

int comp[N];
vector<pt> ng[N];
int us[N][N];
int id[N];

inline void gomoryHu()
{
	int cntC = 1;
	forn (i, n)
		comp[i] = 0;
		
	forn (i, n)
		fore(j, i + 1, n - 1)
			us[i][j] = 0;
			
	int iti = 0;
	int itj = 1;
	
	forn (i, n - 1)
	{
		int s = -1;
		int t = -1;
		while (us[iti][itj])
		{
			itj++;
			
			if (itj == n)
			{
				iti++;
				itj = iti + 1;
			}
		}
		
		s = iti, t = itj;
		
		minCut(s, t);
		
		vector<int> p1, p2;
		
		forn (j, n)
			if (comp[j] == comp[s])
			{
				if (d[j] == INF)
				{
					comp[j] = cntC;
					p2.pb(j);
				}
				else
					p1.pb(j);
			}
			
		forn (x, sz(p1))
			forn (y, sz(p2))
				if (p1[x] < p2[y])
					us[ p1[x] ][ p2[y] ] = 1;
				else
				if (p1[x] > p2[y])
					us[ p2[y] ][ p1[x] ] = 1;
					
		ng[ comp[s] ].pb(mp(cntC, flow));
		ng[cntC].pb(mp(comp[s], flow));
		
		cntC++;		
	}	
    
    forn(i, n)
        id[ comp[i] ] = i;
}