//O(n^3) or O(n^2m)
//about 10 times faster than minCost
//1-indexed matrix A, n - number of mens, m - number of works

inline int venrg(int a[N][N], int n, int m)
{
    char used[N];
    int minv[N];
    int u[N], v[N], p[N], way[N];

    forn (i, max(n, m) + 1)
    {
        u[i] = 0;
     	v[i] = 0;
     	p[i] = 0;
     	way[i] = 0;
    }
     
    for (int i=1; i<=n; ++i) 
    {
        p[0] = i;
        int j0 = 0;
     	
        forn (i, max(n, m) + 1)
        {
            used[i] = 0;
            minv[i] = INF;
        }

        do {
            used[j0] = true;
            int i0 = p[j0],  delta = INF,  j1;
            for (int j=1; j<=m; ++j)
                if (!used[j]) {
                    int cur = a[i0][j]-u[i0]-v[j];
                    if (cur < minv[j])
                        minv[j] = cur,  way[j] = j0;
                    if (minv[j] < delta)
                        delta = minv[j],  j1 = j;
                }
            for (int j=0; j<=m; ++j)
                if (used[j])
                    u[p[j]] += delta,  v[j] -= delta;
                else
                    minv[j] -= delta;
            j0 = j1;
        } while (p[j0] != 0);
        do {
            int j1 = way[j0];
            p[j0] = p[j1];
            j0 = j1;
        } while (j0);
    }
     
    int ans[N];
    for (int j=1; j<=m; ++j)
        ans[ p[j] ] = j;
     
     return -v[0];
}
