const int N = 205; // ����� ������

int n, c[N][N], f[N][N], s, t, d[N], ptr[N], q[N];
 
bool bfs() 
{
	int head = 0, tail = 0;
	q[tail++] = s;
	memset (d, -1, n * sizeof d[0]);
	d[s] = 0;
	
	while (head < tail && d[t] == -1) 
	{
		int v = q[head++];
		forn(to, n)
			if (d[to] == -1 && f[v][to] < c[v][to]) 
			{
				q[tail++] = to;
				d[to] = d[v] + 1;
			}
	}
	
	return d[t] != -1;
}
 
int dfs (int v, int flow) 
{
	if (!flow)  
	    return 0;
	
	if (v == t)  
	    return flow;
	
	for (int &to = ptr[v]; to < n; ++to) 
	{
		if (d[to] != d[v] + 1)  
		    continue;
		int pushed = dfs(to, min2(flow, c[v][to] - f[v][to]));
		
		if (pushed) 
		{
			f[v][to] += pushed;
			f[to][v] -= pushed;
			return pushed;
		}
	}
	
	return 0;
}
 
int dinic() 
{
	int flow = 0;
	while(bfs()) 
	{
		memset (ptr, 0, n * sizeof ptr[0]);
		while (true)
		{
			int pushed = dfs(s, INF);
			flow += pushed;
	
			if (!pushed)
				break;
		}
	}
	
	return flow;
}
