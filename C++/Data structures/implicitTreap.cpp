inline int myRandom ()
{
	int ans = 0;
	forn (it, 2)
	{
		int a = (rand() & ((1 << 15) - 1));
		ans <<= 15;
		ans |= a;
	}

	return ans;
}

struct node
{
	int val;
	int cnt;
	int prior;
	node *l, *r;

	node() { cnt = 1, l = r = NULL, prior = myRandom(); }
};

typedef node* tree;

inline int getCnt (const tree& t)
{
	if (t == NULL)
		return 0;

	return t->cnt;
}

inline void push (tree t)
{
	if (!t)
		return;

	//todo
}

inline void update (tree t)
{
	if (!t)
		return;

	t->cnt = getCnt(t->l) + getCnt(t->r) + 1;
}

void split (tree t, int key, tree& l, tree& r)
{
	if (t == NULL)
	{
		l = r = NULL;
		return;
	}

	push(t);

	if (getCnt(t->l) >= key)
	{
		split(t->l, key, l, t->l);
		r = t;
	}
	else
	{
		split(t->r, key - getCnt(t->l) - 1, t->r, r);
		l = t;
	}

	update(l);
	update(r);
}

tree merge (tree l, tree r)
{
	push(l);
	push(r);

	if (l == NULL)
		return r;
	if (r == NULL)
		return l;

	if (l->prior > r->prior)
	{
		l->r = merge(l->r, r);
		update(l);
		return l;
	}
	else
	{
		r->l = merge(l, r->l);
		update(r);
		return r;
	}
}

inline void someElementOperation (tree& root, int key)
{
	tree t1, t2, t3, t4;
	split(root, key, t1, t2);
	split(t2, 1, t3, t4);

	//todo t3
	
	t2 = merge(t3, t4);
	root = merge(t1, t2);
}

inline void someSegmentOperation (tree& root, int l, int r)
{
	tree t1, t2, t3, t4;
	split(root, l, t1, t2);
	split(t2, r + 1 - l, t3, t4);

	//todo t3
	
	t2 = merge(t3, t4);
	root = merge(t1, t2);
}

tree build (int lf, int rg)
{
	if (lf > rg)
		return NULL;
		
	int mid = (lf + rg) >> 1;
		
	tree res = new node(); //createNewTree(mid)
	res->l = build(lf, mid - 1);
	res->r = build(mid + 1, rg);
	update(res);
	
	if (res->l && res->prior < res->l->prior)
		res->prior = res->l->prior + myRandom();
		
	if (res->r && res->prior < res->r->prior)
		res->prior = res->r->prior + myRandom();
		
	return res;	
}

int szbuf = 0;
node buf[N];