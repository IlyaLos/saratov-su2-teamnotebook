const int N = 100 * 1000 + 13;
int t[N]
int n;

inline int getSum (int r)
{
	int ans = 0;
	for (; r >= 0; r = (r & (r + 1)) - 1)
		ans += t[r];

	return ans;
}

inline void update (int i, int val)
{
	for (; i < n; i = (i | (i + 1)))
		t[i] += val;
}
