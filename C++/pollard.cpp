li n;

const int N = 1000;
int szd = 0;
li divs[N];

inline li myMul (li a, li b, li m)
{
	li q = li(ld(a) * b / m);
	li ans = (a * b - q * m) % m;
	
	if (ans < 0)
		ans += m;
		
	return ans;
}

inline li binPow (li a, li b, li m)
{
	li ans = 1;

	while (b)
	{
		if (b & 1ll)
			ans = myMul(ans, a, m);
			
		a = myMul(a, a, m);
		b >>= 1;
	}
	
	return ans;
}

inline li next (li a, li q, li p)
{
	return (myMul(a, a, p) + q) % p;
}

inline li myRand ()
{
	li ans = 0;
	forn (it, 4)
		ans = (ans << 15) ^ (abs(rand()) & ((1 << 15) - 1));
		
	return ans;
}

inline bool isPropablePrime (li n)
{
	if (n == 1)
		return false;

	if (n == 2)
		return true;

	if (!(n & 1))
		return false;		

	const int M = 19;
	
	forn (it, M)
	{
		li a = myRand() % (n - 1) + 1;
		li b = n - 1;
		
		int cnt = 0;
		while (!(b & 1ll))
			b >>= 1ll, cnt++;
			
		li ans = binPow(a, b, n);
			
		forn (j, cnt)
		{
			li next = myMul(ans, ans, n);
			
			if (next == 1 && ans != 1 && ans != n - 1)
				return false;
				
			ans = next;
		}			
		
		if (ans != 1)
			return false;
	}
	
	return true;
}

li gcd (li a, li b)
{
	if (b == 0)
		return a;
		
	return gcd(b, a % b);
}

void pollard (li n)
{
	if (isPropablePrime(n))
	{                 
		divs[szd++] = n;
		return;
	}

	if (n == 1)
		return;

	if (!(n & 1))
	{
		pollard(2);
		pollard(n >> 1);

		return;
	}		

	if (n < 1000 * 1000)
	{
		for (int i = 2; i * i <= n; i++)
			if (n % i == 0)
			{
				pollard(i);
				pollard(n / i);
				
				return;
			}
			
		throw;
	}
		
	const int M = 5;
	static li a[M], b[M], q[M];
	
	forn (i, M)
	{
		a[i] = b[i] = 2;
		q[i] = myRand() % n;
	}
	
	q[0] = 1;
	
	int step = 1;
	for (int i = 1; ; i++)
	{
		forn (j, M)
		{
			li d = gcd(abs(a[j] - b[j]), n);
			
			if (d != 1 && d != n)
			{
				pollard(n / d);
				pollard(d);
				
				return;
			}
			
			b[j] = next(b[j], q[j], n);
		}
		
		if (i == step)
		{
			forn (j, M)
			{
				a[j] = b[j];
				b[j] = next(b[j], q[j], n);
			}
			
			step <<= 1;
		}
	}
	
	throw;
}
