const int N = 1000 * 1000 + 13;
int n;
pt a[N];

ld ans = 1e18;

inline ld getDist (const pt& a, const pt& b)
{
    return sqrt(sqr(ld(a.x - b.x)) + sqr(ld(a.y - b.y)));
}

inline void updateAns (const pt& a, const pt& b, const pt& c)
{
    ld res = getDist(a, b) + getDist(b, c) + getDist(a, c);
    
    ans = min(ans, res);
}

inline bool cmp (const pt& a, const pt& b)
{
    return a.y < b.y;
}

pt t[N];
pt tmp[N];

void calc (int l, int r)
{
    if (r - l + 1 <= 3)
    {
        if (r - l + 1 == 3)
            updateAns(a[l], a[l + 1], a[l + 2]);
                    
        sort(a + l, a + r + 1, cmp);
                    
        return;
    }
        
    int mid = (r + l) >> 1;
    int midX = a[mid].x;

    calc(l, mid);
    calc(mid + 1, r); 
    
    //inplace_merge(a + l, a + mid + 1, a + r + 1, cmp);
	{
		int it1 = l, it2 = mid + 1;
		fore (i, l, r)
		{
			if (it1 == mid + 1)
				tmp[i] = a[it2++];
			else
			if (it2 == r + 1)
				tmp[i] = a[it1++];
			else
			if (cmp(a[it1], a[it2]))
				tmp[i] = a[it1++];
			else
				tmp[i] = a[it2++];
		}

		fore(i, l, r)
			a[i] = tmp[i];
	}
    
    int cnt = 0;
    
    for (int i = l; i <= r; i++)
    {
        if (abs(a[i].x - midX) > ans * 0.5 + EPS)
            continue;
            
        for (int j = cnt - 1; j >= 0 && abs(a[i].y - t[j].y) < ans * 0.5 + EPS; j--)
            for (int k = j - 1; k >= 0 && abs(a[i].y - t[k].y) < ans * 0.5 + EPS; k--)
                updateAns(a[i], t[j], t[k]);
                
        t[cnt++] = a[i];
    }
}   

inline ld calcMinPerimetr ()
{
    sort(a, a + n);
    ans = 1e18;
    
    calc(0, n - 1);
    
    return ans;
}
