int n, szans;
pt p[N], ans[N];

inline pt operator - (const pt& a, const pt& b) { return mp(a.x - b.x, a.y - b.y); }
inline li dist2 (const pt& a, const pt& b) { return sqr(li(a.x - b.x)) + sqr(li(a.y - b.y)); }
inline li cross (const pt& a, const pt& b) { return a.x * 1LL * b.y - a.y * 1LL * b.x; }

pt pole;

inline bool cmp (const pt& a, const pt& b)
{
	li d = cross(a - pole, b - pole);

	if (d != 0) 
		return d > 0;
	
	return dist2(a, pole) < dist2(b, pole);
}

inline void convexHull ()
{
	int idx = 0;
	
	forn(i, n)
		if (p[i].y < p[idx].y || (p[i].y == p[idx].y && p[i].x < p[idx].x))
			idx = i;
	
	swap(p[idx], p[0]);
	pole = p[0];
	
	sort(p + 1, p + n, cmp);
	
	forn(i, n)
	{
		while (szans >= 2 && cross(ans[szans - 1] - ans[szans - 2], p[i] - ans[szans - 2]) <= 0) 
			szans--;
			
		ans[szans++] = p[i];
	}
}
