inline ld getS(const ld& r, const ld& x)
{
    ld y = sqrt(max(ld(0), sqr(r) - sqr(x)));
    ld ang = asinl(x / r) * 2 + PI;
    return PI * sqr(r) - y * x - (sqr(r) * ang / 2);
}

inline ld squareCircleCircleIntersect(pt a1, ld r1, pt a2, ld r2)
{
    if (r1 < r2 - EPS)
    {
        swap(a1, a2);
        swap(r1, r2);
    }
    
    ld len2 = sqr(a1.x - a2.x) + sqr(a1.y - a2.y);
    ld len = sqrt(len2);

    if (len + EPS >= r1 + r2)
        return 0;
    if (len - EPS <= r1 - r2)
        return PI * sqr(r2);

    ld x = (sqr(r2) - sqr(r1) - len2) / (ld(-2) * len);
    return getS(r1, x) + getS(r2, len - x);
}
