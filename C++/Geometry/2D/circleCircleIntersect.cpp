const ld EPS = 1e-12;

inline bool cmpEq (const pt& a, const pt& b)
{
 	return abs(a.x - b.x) < EPS && abs(a.y - b.y) < EPS;
}

int szlc = 0;
pt lc[5];

inline void intersectLineCricle (const ld& A, const ld& B, ld C, const pt& a, const ld& r)
{
	C += A * a.x + B * a.y;
	szlc = 0;

	ld div = sqr(A) + sqr(B);

	ld x0 = -(A * C) / div, y0 = -(B * C) / div;

	ld val = sqr(r) * div;

	if (sqr(C) > val + EPS)
	{
		return;
	}
	else
	if (abs(sqr(C) - val) < EPS)
	{
		lc[szlc++] = mp(x0, y0) + a;
	}
	else
	{
		ld d = sqr(r) - sqr(C) / div;
		ld mult = sqrt(d / div);

		pt p1, p2;
		p1.x = x0 + B * mult;
		p2.x = x0 - B * mult;
		p1.y = y0 - A * mult;
		p2.y = y0 + A * mult;

		lc[szlc++] = p1 + a;
		lc[szlc++] = p2 + a;
	}
}

int szcc;
pt cc[5];

inline void intersectCircleCircle(pt a1, ld r1, pt a2, ld r2)
{
	szcc = 0;

	if (cmpEq(a1, a2))
	{
		if (abs(r1 - r2) < EPS)
			//infinity

		return;
	}

	a2 = a2 - a1;

	ld A = -2 * a2.x, B = -2 * a2.y, C = sqr(a2.x) + sqr(a2.y) - sqr(r2) + sqr(r1);

	intersectLineCricle(A, B, C, mp(0, 0), r1);

	forn (i, szlc)
		cc[szcc++] = lc[i] + a1;
}