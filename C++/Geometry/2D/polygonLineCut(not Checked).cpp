typedef pair<ld, ld> pt;

inline int sign(const ld& a)
{
	if (abs(a) < EPS)
		return 0;

	return (a < 0) ? -1 : 1;
}

inline pair< vector<pt>, vector<pt> > cut(const vector<pt>& p, const pt& a, const pt& b)
{
	ld A = a.y - b.y, B = b.x - a.x;
	ld C = -(A * a.x + B * a.y);

	pair< vector<pt>, vector<pt> > res;

	forn (i, sz(p))
	{
		int j = (i + 1 == sz(p)) ? 0 : (i + 1);

		int si = sign(A * p[i].x + B * p[i].y + C);
		int sj = sign(A * p[j].x + B * p[j].y + C);

		if (si <= 0)
			res.ft.pb(p[i]);
		if (si >= 0)
			res.sc.pb(p[i]);

		if (si * sj < 0)
		{
			pt r;
			assert(intersectSegmentLine(p[i], p[j], a, b, r)); 

			res.ft.pb(r);
			res.sc.pb(r);
		}
	}

	return res;
}