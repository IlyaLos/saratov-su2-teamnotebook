const ld EPS = 1e-10;

inline bool cmpEq (const pt& a, const pt& b)
{
 	return abs(a.x - b.x) < EPS && abs(a.y - b.y) < EPS;
}

inline bool in (ld a, ld b, ld p)
{
 	return min(a, b) < p + EPS && max(a, b) > p - EPS;
}

inline bool in (const pt& a, const pt& b, const pt& p)
{
	return in(a.x, b.x, p.x) && in(a.y, b.y, p.y);
}

int szlc = 0;
pt lc[5];

inline void intersectLineCricle (const ld& A, const ld& B, ld C, const pt& a, const ld& r)
{
	C += A * a.x + B * a.y;
	szlc = 0;

	ld div = sqr(A) + sqr(B);

	ld x0 = -(A * C) / div, y0 = -(B * C) / div;

	ld val = sqr(r) * div;

	if (sqr(C) > val + EPS)
	{
		return;
	}
	else
	if (abs(sqr(C) - val) < EPS)
	{
		lc[szlc++] = mp(x0, y0) + a;
	}
	else
	{
		ld d = sqr(r) - sqr(C) / div;
		ld mult = sqrt(d / div);

		pt p1, p2;
		p1.x = x0 + B * mult;
		p2.x = x0 - B * mult;
		p1.y = y0 - A * mult;
		p2.y = y0 + A * mult;

		lc[szlc++] = p1 + a;
		lc[szlc++] = p2 + a;
	}
}

int szsc;
pt sc[5];

inline void intersectSegmentCircle(const pt& a, const pt& b, const pt& p, const ld& r)
{
	szsc = 0;

	if (cmpEq(a, b))
	{
		if (abs(sqr(a.x - p.x) + sqr(a.y - p.y) - sqr(r)) < EPS)
			sc[szsc++] = a;

		return;
	}

	ld A = a.y - b.y, B = b.x - a.x;
	ld C = -(A * a.x + B * a.y);

	intersectLineCricle(A, B, C, p, r);

	forn (i, szlc)
		if (in(a, b, lc[i]))
			sc[szsc++] = lc[i];
}