struct seg {
	pt p, q;
	int id;

	inline ld get_y (const ld x) const
	{
		if (abs(p.x - q.x) < EPS)
			return p.y;

		return p.y + (q.y - p.y) * (x - p.x) / (q.x - p.x);
	}
};

inline bool intersect1d (ld l1, ld r1, ld l2, ld r2)
{
	if (l1 > r1)
		swap (l1, r1);

	if (l2 > r2)
		swap (l2, r2);

	return max (l1, l2) <= min (r1, r2) + EPS;
}

inline int vec (const pt& a, const pt& b, const pt& c)
{
	ld s = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);

	return (abs(s) < EPS) ? 0 : ((s > 0) ? +1 : -1);
}

inline bool intersect (const seg& a, const seg& b)
{
	return intersect1d (a.p.x, a.q.x, b.p.x, b.q.x)
		&& intersect1d (a.p.y, a.q.y, b.p.y, b.q.y)
		&& vec (a.p, a.q, b.p) * vec (a.p, a.q, b.q) <= 0
		&& vec (b.p, b.q, a.p) * vec (b.p, b.q, a.q) <= 0;
}


inline bool operator < (const seg& a, const seg& b)
{
	ld x = max(min(a.p.x, a.q.x), min(b.p.x, b.q.x));

	return a.get_y(x) < b.get_y(x) - EPS;
}

struct event
{
	ld x;
	int tp, id;

	event() { }
	event (const ld& x, const int& tp, const int& id) : x(x), tp(tp), id(id) { }

	inline bool operator < (const event& e) const {
		if (abs(x - e.x) > EPS)
			return x < e.x;
		return tp > e.tp;
	}
};

set<seg> s;
vector < set<seg>::iterator > where;

inline set<seg>::iterator prev (set<seg>::iterator it)
{
	return it == s.begin() ? s.end() : --it;
}

inline set<seg>::iterator next (set<seg>::iterator it)
{
	return ++it;
}

const int N = 125 * 1000 + 13;
int n;
seg a[N];
int sze = 0;
event e[2 * N];

inline pair<int, int> calc ()
{
	sze = 0;
	for (int i=0; i<n; ++i) {
		e[sze++] = event (min (a[i].p.x, a[i].q.x), +1, i);
		e[sze++] = event (max (a[i].p.x, a[i].q.x), -1, i);
	}

	sort(e, e + sze);

	s.clear();
	where.resize(n);
	forn (i, sze)
	{
		int id = e[i].id;

		if (e[i].tp == +1)
		{
			set<seg>::iterator nxt = s.lower_bound(a[id]), prv = prev(nxt);

			if (nxt != s.end() && intersect(*nxt, a[id]))
				return mp(nxt->id, id);

			if (prv != s.end() && intersect(*prv, a[id]))
				return mp(prv->id, id);

			where[id] = s.insert(nxt, a[id]);
		}
		else
		{
			set<seg>::iterator nxt = next(where[id]), prv = prev(where[id]);

			if (nxt != s.end() && prv != s.end() && intersect(*nxt, *prv))
				return mp(prv->id, nxt->id);

			s.erase(where[id]);
		}
	}

	return mp(-1, -1);
}
