typedef pair<ld, ld> pt;

inline pt operator + (const pt& a, const pt& b) { return mp(a.x + b.x, a.y + b.y); }
inline pt operator - (const pt& a, const pt& b) { return mp(a.x - b.x, a.y - b.y); }
inline pt operator * (const pt& a, const ld& b) { return mp(a.x * b, a.y * b); }
inline pt operator / (const pt& a, const ld& b) { return mp(a.x / b, a.y / b); }

inline ld len (const pt& v) { return sqrt(sqr(v.x) + sqr(v.y)); }

inline void norm (pt& v, ld k) {
	v = v / len(v);
	v = v * k;
}

inline pt rotate (const pt& v, ld ang) {
	return mp(v.x * cos(ang) - v.y * sin(ang), v.x * sin(ang) + v.y * cos(ang));
}

inline pt rotate (const pt& v, ld sina, ld cosa) {
	return mp(v.x * cosa - v.y * sina, v.x * sina + v.y * cosa);
}

inline ld getDist (const pt& a, const pt& b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

inline bool onLine (const ld& A, const ld& B, const ld& C, const pt& p) {
	return abs(A * p.x + B * p.y + C) < EPS;
}

inline int getSign (const ld& A, const ld& B, const ld& C, const pt& p) {
	ld val = A * p.x + B * p.y + C;

	if (abs(val) < EPS)
		return 0;

	if (val > EPS)
		return 1;

	if (val < -EPS)
		return -1;

	throw;
}

inline bool in (const ld& a, const ld& b, const ld& p) //inclusive!!
{
	return min(a, b) <= p + EPS && max(a, b) >= p - EPS;
}

inline bool in (const pt& a, const pt& b, const pt& p)
{
	return in(a.x, b.x, p.x) && in(a.y, b.y, p.y);
}

inline ld det (const ld& a, const ld& b, const ld& c, const ld& d)
{
	return a * d - b * c;
}

inline bool intersect (const pt& a, const pt& b, const pt& c, const pt& d, pt& p)
{
	ld A1 = a.y - b.y, B1 = b.x - a.x;
	ld C1 = -(A1 * a.x + B1 * a.y);
	ld A2 = c.y - d.y, B2 = d.x - c.x;
	ld C2 = -(A2 * c.x + B2 * c.y);

	ld D = det(A1, B1, A2, B2);

	if (abs(D) < EPS) //Warning!!
		return false;

	p.x = -det(C1, B1, C2, B2) / D;
	p.y = -det(A1, C1, A2, C2) / D;

	return in(a, b, p) && in(c, d, p);
}

inline ld triangle_area_2 (const pt& a, const pt& b, const pt& c) { //sign area
	return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
}

inline ld triangle_area (const pt& a, const pt& b, const pt& c) { //area
	return abs(triangle_area_2(a, b, c)) / ld(2);
}

inline bool clockwise (const pt& a, const pt& b, const pt& c) {
	return triangle_area_2 (a, b, c) < -EPS;
}

inline bool counter_clockwise (const pt& a, const pt& b, const pt& c) {
	return triangle_area_2 (a, b, c) > EPS;
}