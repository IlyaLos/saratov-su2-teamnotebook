const ld EPS = 1e-10;

int szlc = 0;
pt lc[5];

inline void intersectLineCricle (const ld& A, const ld& B, ld C, const pt& a, const ld& r)
{
	C += A * a.x + B * a.y;
	szlc = 0;

	ld div = sqr(A) + sqr(B);

	ld x0 = -(A * C) / div, y0 = -(B * C) / div;

	ld val = sqr(r) * div;

	if (sqr(C) > val + EPS)
	{
		return;
	}
	else
	if (abs(sqr(C) - val) < EPS)
	{
		lc[szlc++] = mp(x0, y0) + a;
	}
	else
	{
		ld d = sqr(r) - sqr(C) / div;
		ld mult = sqrt(d / div);

		pt p1, p2;
		p1.x = x0 + B * mult;
		p2.x = x0 - B * mult;
		p1.y = y0 - A * mult;
		p2.y = y0 + A * mult;

		lc[szlc++] = p1 + a;
		lc[szlc++] = p2 + a;
	}
}