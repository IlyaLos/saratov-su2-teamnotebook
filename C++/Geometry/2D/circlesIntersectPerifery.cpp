const int N = 100 + 13;
int n;
pt a[N];
int r[N];

inline bool in (const pt& a, int r, const ptd& p)
{
	return sqr(a.x - p.x) + sqr(a.y - p.y) <= sqr(r) + EPS;
}

inline bool inAll (const ptd& p)
{
	forn (i, n)
		if (!in(a[i], r[i], p))
			return false;

	return true;
}

inline ptd operator + (const ptd& a, const ptd& b) { return mp(a.x + b.x, a.y + b.y); }
inline ptd operator - (const ptd& a, const ptd& b) { return mp(a.x - b.x, a.y - b.y); }
inline ptd operator * (const ptd& a, const ld& b) { return mp(a.x * b, a.y * b); }
inline ptd operator / (const ptd& a, const ld& b) { return mp(a.x / b, a.y / b); }

inline void norm (ptd& v, ld len)
{
	ld k = sqrt(sqr(v.x) + sqr(v.y));

	v = v / k;
	v = v * len;
}

inline ptd rotate(const ptd& v, ld ang) { return mp(v.x * cos(ang) - v.y * sin(ang), v.x * sin(ang) + v.y * cos(ang)); }

inline bool onLine (li A, li B, li C, const ptd& p)
{
	return abs(A * p.x + B * p.y + C) < EPS;
}

int szp;
ptd p[N];

inline void intersectLineCircle(int r, li A, li B, li C)
{
	ld dist = ld(abs(C)) / sqrt(ld(sqr(A) + sqr(B)));

	if (dist > ld(r) + EPS)
		return;

	ptd v = mp(ld(A), ld(B));
	norm(v, ld(r));

	forn (it, 2)
	{
		ld ang = acosl(dist / ld(r));

		ptd nv = rotate(v, ang);

		if (onLine(A, B, C, nv))
			p[szp++] = nv;

		nv = rotate(v, -ang);

		if (onLine(A, B, C, nv))
			p[szp++] = nv;

		v = v * (-1);
	}
}

inline bool cmpLess (const ptd& a, const ptd& b)
{
	if (abs(a.x - b.x) < EPS)
		return a.y < b.y;

	return a.x < b.x;
}

inline bool cmpEq (const ptd& a, const ptd& b)
{
	return abs(a.x - b.x) < EPS && abs(a.y - b.y) < EPS;
}

inline void intersectCircles(const pt& a, int r1, pt b, int r2)
{
	b = b - a;
	li A = -2 * b.x, B = -2 * b.y, C = sqr(b.x) + sqr(b.y) + sqr(r1) - sqr(r2);
	
	intersectLineCircle(r1, A, B, C);

	sort(p, p + szp, cmpLess);
	szp = int(unique(p, p + szp, cmpEq) - p);

	forn (i, szp)
		p[i] = p[i] + a;
}

pt pole;

inline bool cmpAng (const ptd& a, const ptd& b)
{
	return atan2(a.y - pole.y, a.x - pole.x) < atan2(b.y - pole.y, b.x - pole.x);
}

inline ld getDist(const pt& a, const pt& b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

inline bool onCircle(const pt& a, ld r1, const pt& b, ld r2)
{
	ld dist = getDist(a, b);

	if (r1 >= r2 + EPS)
		return false;

	return dist <= (r2 - r1) + EPS;
}

vector<ptd> pc[N];

inline ld calcPeriferyUnionOfCircles()
{
	forn (j, n)
		forn (i, n)
			if (i != j && onCircle(a[i], r[i], a[j], r[j]))
			{
				swap(a[n - 1], a[j]);
				swap(r[n - 1], r[j]);

				n--;
				j--;

				break;
			}
			
	if (n == 1)
		return ld(2) * PI * r[0];
	
	forn (i, n)
		pc[i].clear();

	forn (i, n)
		fore(j, i + 1, n - 1)
		{
			szp = 0;
			intersectCircles(a[i], r[i], a[j], r[j]);

			forn (k, szp)
			{
				if (inAll(p[k]))
				{
					pc[i].pb(p[k]);
					pc[j].pb(p[k]);
				}

                assert(abs(sqr(p[k].x - a[i].x) + sqr(p[k].y - a[i].y) - sqr(r[i])) < EPS);
				assert(abs(sqr(p[k].x - a[j].x) + sqr(p[k].y - a[j].y) - sqr(r[j])) < EPS);
			}
		}

	ld ans = 0;

	forn (i, n)
	{
		pole = a[i];

		sort(all(pc[i]), cmpAng);
		pc[i].erase(unique(all(pc[i]), cmpEq), pc[i].end());

		if (sz(pc[i]) <= 1)
			continue;

		forn (j, sz(pc[i]))
		{
			int next = j + 1;
			if (next == sz(pc[i]))
				next = 0;

			ptd pl = pc[i][j], pr = pc[i][next];

			ld l = atan2(pl.y - pole.y, pl.x - pole.x) + 2 * PI;
			ld r = atan2(pr.y - pole.y, pr.x - pole.x) + 2 * PI;

			if (r < l - EPS)
				r += ld(2) * PI;

			ld mid = (r + l) / ld(2);

			ptd p = pole + mp(cos(mid) * ::r[i], sin(mid) * ::r[i]);

			if (inAll(p))
				ans += (r - l) * ::r[i];
		}
	}
	
	return ans;
}
