//!!
//целочисленные точки

const int N = 100 * 1000 + 13;

struct point
{
	pt a;
	int id;
	
	point () {}
	
	point (int x, int y, int Id)
	{
		id = Id;
		a = mp(x, y);
	}
};

int n;
point a[N];

inline bool cmp (const pt& a, const pt& b)
{
	return a.Y > b.Y;
}

inline bool cmp2 (const point& a, const point& b)
{
	return a.a < b.a;
}

li ans;
pt res;

inline void update (const point& a, const point& b)
{
	li dist = sqr(li(a.a.X - b.a.X)) + sqr(li(a.a.Y - b.a.Y));
	
	if (dist < ans)
	{
		ans = dist;
		res = mp(a.id, b.id);
	}
}

point tmp[N];

inline void myMerge (int l, int r)
{
	int mid = (l + r) >> 1;
	
	int it1 = l, it2 = mid + 1;
	
	for (int i = 0; i < r - l + 1; i++)
	{
		if (it1 > mid)
			tmp[i] = a[it2++];
		else
		if (it2 > r)
			tmp[i] = a[it1++];
		else
		{
			if (cmp(a[it1].a, a[it2].a))
				tmp[i] = a[it2++];
			else
				tmp[i] = a[it1++];
		}		
	}
	
	for (int i = 0; i < r - l + 1; i++)
		a[i + l] = tmp[i];
}

void calc (int l, int r)
{
	if (l >= r)
		return;
		
	if (r - l == 1)
	{
		update(a[l], a[r]);
		
		if (cmp(a[l].a, a[r].a))
			swap(a[l], a[r]);
		
		return;
	}
	
	int mid = (r + l) >> 1;
	int h = a[mid].a.X;
	
	calc(l, mid);
	calc(mid + 1, r);
	
	myMerge(l, r);
	
	int it = 0;
	
	for (int i = l; i <= r; i++)
	{
		if (sqr(li(a[i].a.X - h)) >= ans)
			continue;

		for (int j = it - 1; j >= 0 && sqr(li(a[i].a.Y - tmp[j].a.Y)) < ans; j--)
			update(a[i], tmp[j]);	
			
		tmp[it++] = a[i];
	}
}

inline void calcNearestPoints ()
{
	sort(a, a + n, cmp2);
	
	ans = INF64;
	
	calc(0, n - 1);
}
