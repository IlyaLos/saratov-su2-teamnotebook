inline bool cmp (const pt& a, const pt& b)
{
	return a < b; //!!! EPS
}

inline bool cw (const pt& a, const pt& b, const pt& c) 
{
	return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) < 0; //!!! EPS
}

inline bool ccw (const pt& a, const pt& b, const pt& c) 
{
	return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) > 0;  //!!! EPS
}

int szup, szdown;
pt up[N], down[N];

inline void convex_hull () 
{
	sort (a, a + n, cmp);
	pt p1 = a[0],  p2 = a[n - 1];
	
	szup = szdown = 0;
	up[szup++] = p1;
	down[szdown++] = p1;
	
	for (int i = 1; i < n; i++) 
	{
		if (i == n - 1 || cw(p1, a[i], p2)) 
		{
			while (szup >= 2 && !cw(up[szup - 2], up[szup - 1], a[i]))
			    szup--;
			    
			up[szup++] = a[i];
		}
 
 		if (i == n - 1 || ccw(p1, a[i], p2)) 
		{
			while (szdown >= 2 && !ccw(down[szdown - 2], down[szdown - 1], a[i]))
				szdown--;
				
			down[szdown++] = a[i];
		}
	}
	
	n = 0;
	
	forn (i, szup)
		a[n++] = up[i];
		
	for (int i = szdown - 2; i > 0; i--)
		a[n++] = down[i];
}