inline li area (const pt& a, const pt& b, const pt& c)
{
	return (b.x - a.x) * 1ll * (c.y - a.y) - (b.y - a.y) * 1ll * (c.x - a.x);
}

inline bool intersect (int a, int b, int c, int d)
{
	if (a > b)
		swap (a, b);
	if (c > d)
		swap (c, d);

	return max(a, c) <= min(b, d);
}

inline int sign (li val)
{
	return (val == 0 ? 0 : (val < 0 ? -1 : 1));
}

inline bool intersect (const pt& a, const pt& b, const pt& c, const pt& d)
{
	return intersect (a.x, b.x, c.x, d.x) && intersect (a.y, b.y, c.y, d.y)
		&& sign(area(a, b, c)) * sign(area(a, b, d)) <= 0 && sign(area(c, d, a)) * sign(area(c, d, b)) <= 0;
}
