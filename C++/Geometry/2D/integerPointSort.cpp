inline li cross(const pt& a, const pt& b) {
	return a.x * 1ll * b.y - a.y * 1ll * b.x;
}

inline int side(const pt& a) {
	if (a.y == 0)
		return a.x > 0 ? 0 : 2;
	return a.y > 0 	? 1 : 3;
}

inline bool cmpClockwise(const pt& a, const pt& b) {
	int sa = side(a);
	int sb = side(b);
	if (sa != sb) {
		return sa < sb;
	}
	return cross(a, b) > 0;
}
