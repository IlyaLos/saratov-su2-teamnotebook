inline pt operator + (const pt& a, const pt& b) { return mp(a.x + b.x, a.y + b.y); }
inline pt operator - (const pt& a, const pt& b) { return mp(a.x - b.x, a.y - b.y); }
inline pt operator * (const pt& a, const ld& b) { return mp(a.x * b, a.y * b); }
inline pt operator / (const pt& a, const ld& b) { return mp(a.x / b, a.y / b); }

inline pt rotate(const pt& v, ld ang) { return mp(v.x * cos(ang) - v.y * sin(ang), v.x * sin(ang) + v.y * cos(ang)); }

const int N = 100 + 13;
int n;
ptd a[N];
ld r[N];

inline bool in (const pt& a, ld r, const pt& p)
{
	return sqr(a.x - p.x) + sqr(a.y - p.y) <= sqr(r) + EPS;
}

inline bool inAll (const pt& p)
{
	forn (i, n)
		if (!in(a[i], r[i], p))
			return false;

	return true;
}

inline void norm (pt& v, ld len)
{
	ld k = sqrt(sqr(v.x) + sqr(v.y));

	v = v / k;
	v = v * len;
}


inline bool onLine (ld A, ld B, ld C, const pt& p)
{
	return abs(A * p.x + B * p.y + C) < EPS;
}

int szp;
pt p[N];

inline void intersectLineCircle(ld r, ld A, ld B, ld C)
{
	ld dist = ld(abs(C)) / sqrt(ld(sqr(A) + sqr(B)));

	if (dist > ld(r) + EPS)
		return;

	pt v = mp(ld(A), ld(B));
	norm(v, ld(r));

	forn (it, 2)
	{
		ld ang = acosl(dist / ld(r));

		pt nv = rotate(v, ang);

		if (onLine(A, B, C, nv))
			p[szp++] = nv;

		nv = rotate(v, -ang);

		if (onLine(A, B, C, nv))
			p[szp++] = nv;

		v = v * (-1);
	}
}

inline bool cmpLess (const pt& a, const pt& b)
{
	if (abs(a.x - b.x) < EPS)
		return a.y < b.y;

	return a.x < b.x;
}

inline bool cmpEq (const pt& a, const pt& b)
{
	return abs(a.x - b.x) < EPS && abs(a.y - b.y) < EPS;
}

inline void intersectCircles(const pt& a, ld r1, pt b, ld r2)
{
	b = b - a;
	ld A = -2 * b.x, B = -2 * b.y, C = sqr(b.x) + sqr(b.y) + sqr(r1) - sqr(r2);

	intersectLineCircle(r1, A, B, C);

	sort(p, p + szp, cmpLess);
	szp = int(unique(p, p + szp, cmpEq) - p);

	forn (i, szp)
		p[i] = p[i] + a;
}

pt pole;

inline bool cmpAng (const pt& a, const pt& b)
{
	return atan2(a.y - pole.y, a.x - pole.x) < atan2(b.y - pole.y, b.x - pole.x);
}

inline ld getDist(const pt& a, const pt& b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

inline bool onCircle(const pt& a, ld r1, const pt& b, ld r2)
{
	ld dist = getDist(a, b);

	if (r1 >= r2 + EPS)
		return false;

	return dist <= (r2 - r1) + EPS;
}

vector<pt> pc[N];
int szAll = 0;
pt allP[N * N];

inline ld calcSquareOsUinon()
{
	forn (j, n)
		forn (i, n)
			if (i != j && onCircle(a[i], r[i], a[j], r[j]))
			{
				swap(a[n - 1], a[j]);
				swap(r[n - 1], r[j]);

				n--;
				j--;

				break;
			}
			
	if (n == 1)
		return PI * sqr(r[0]);
	
	forn (i, n)
		pc[i].clear();
	szAll = 0;

	forn (i, n)
		fore(j, i + 1, n - 1)
		{
			szp = 0;
			intersectCircles(a[i], r[i], a[j], r[j]);

			forn (k, szp)
			{
				if (inAll(p[k]))
				{
					pc[i].pb(p[k]);
					pc[j].pb(p[k]);

					allP[szAll++] = p[k];
				}

				assert(abs(sqr(p[k].x - a[i].x) + sqr(p[k].y - a[i].y) - sqr(r[i])) < EPS);
				assert(abs(sqr(p[k].x - a[j].x) + sqr(p[k].y - a[j].y) - sqr(r[j])) < EPS);
			}
		}
		
	sort(allP, allP + szAll, cmpLess);
	szAll = int(unique(allP, allP + szAll, cmpEq) - allP);
		
	pole = mp(0, 0);
	forn (i, szAll)
		pole = pole + allP[i];
		
	pole = pole / szAll;
	
	sort(allP, allP + szAll, cmpAng);

	ld ans = 0;

	forn (i, szAll)
	{
		int next = (i + 1) % szAll;

		ans += (allP[next].x - allP[i].x) * (allP[next].y + allP[i].y);
	}

	ans /= ld(2);
	ans = abs(ans);

	forn (i, n)
	{
		pole = a[i];

		sort(all(pc[i]), cmpAng);
		pc[i].erase(unique(all(pc[i]), cmpEq), pc[i].end());

		if (sz(pc[i]) <= 1)
			continue;

		forn (j, sz(pc[i]))
		{
			int next = j + 1;
			if (next == sz(pc[i]))
				next = 0;

			pt pl = pc[i][j], pr = pc[i][next];

			ld l = atan2(pl.y - pole.y, pl.x - pole.x) + 2 * PI;
			ld r = atan2(pr.y - pole.y, pr.x - pole.x) + 2 * PI;

			if (l < -EPS)
				l += 2 * PI;
			if (r < -EPS)
				r += 2 * PI;

			if (r < l - EPS)
				r += ld(2) * PI;

			ld mid = (r + l) / ld(2);

			pt p = pole + mp(cos(mid) * ::r[i], sin(mid) * ::r[i]);

			if (inAll(p))
			{
				ans += sqr(::r[i]) * (r - l) / ld(2);
				ans -= sqr(::r[i]) * sin(r - l) / ld(2);
			}
		}
	}

	return ans;
}
