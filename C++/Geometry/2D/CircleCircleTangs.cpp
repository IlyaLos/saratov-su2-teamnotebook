//EPS = 1e-11

struct line
{
	ld A;
	ld B;
	ld C;
	
	line() {}
	line(const ld& A, const ld& B, const ld& C) : A(A), B(B), C(C) {}
	line(const pt& a, const pt& b)
	{
		A = a.y - b.y;
		B = b.x - a.x;
		C = -(A * a.x + B * a.y);
	}	
	
	void normal()
	{
     	ld k = sqrt(sqr(A) + sqr(B));
     	
     	assert(abs(k) > EPS);
     	
     	A /= k;
     	B /= k;
     	C /= k;
     	
     	if (A < -EPS || (abs(A) < EPS && B < -EPS))
     		A = -A, B = -B, C = -C;		
	}
};

inline bool operator < (const line& a, const line& b)
{
	if (abs(a.A - b.A) > EPS)
		return a.A < b.A;
		
	if (abs(a.B - b.B) > EPS)
		return a.B < b.B;
		
	return a.C < b.C - EPS;
}

inline ld getDist (const pt& a, const pt& b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

inline void norm (pt& a, const ld& len)
{
	ld k = sqrt(sqr(a.x) + sqr(a.y));
	
	if (abs(k) < EPS)
		return;
	
	a = a / k;
	a = a * len;
}

int szlc = 0;
pt lc[5];

inline void intersectLineCricle (const ld& A, const ld& B, ld C, const pt& a, const ld& r)
{
	C += A * a.x + B * a.y;
	szlc = 0;

	ld div = sqr(A) + sqr(B);

	ld x0 = -(A * C) / div, y0 = -(B * C) / div;

	ld val = sqr(r) * div;

	if (sqr(C) > val + EPS)
	{
		return;
	}
	else
	if (abs(sqr(C) - val) < EPS)
	{
		lc[szlc++] = mp(x0, y0) + a;
	}
	else
	{
		ld d = sqr(r) - sqr(C) / div;
		ld mult = sqrt(d / div);

		pt p1, p2;
		p1.x = x0 + B * mult;
		p2.x = x0 - B * mult;
		p1.y = y0 - A * mult;
		p2.y = y0 + A * mult;

		lc[szlc++] = p1 + a;
		lc[szlc++] = p2 + a;
	}
}

vector<line> tangs;
vector<pt> tangP;

inline void calc (const pt& p, const pt& a, const ld& r)
{
	tangP.clear();
	
	ld d = getDist(p, a);

	if (d < r - EPS)
		return;
		
	if (abs(d - r) < EPS)
	{
		pt v = p - a;
		norm(v, r);
		
		tangP.pb(v + a);
		
		return;
	}
	
	intersectLineCricle(p.x - a.x, p.y - a.y, -sqr(r), mp(0, 0), r);
	
	tangP.resize(szlc);
	
	forn (i, szlc)
		tangP[i] = lc[i] + a;
}

inline void calcOut (ld x1, ld y1, ld x2, ld y2, ld r1, ld r2)
{
	if (abs(r1 - r2) < EPS)
	{
		line cur = line(mp(x1, y1), mp(x2, y2));
		
		pt v = mp(cur.A, cur.B);
		norm(v, r1);
		
		tangs.pb(line(cur.A, cur.B, cur.C - (cur.A * v.x + cur.B * v.y)));
		tangs.pb(line(cur.A, cur.B, cur.C + (cur.A * v.x + cur.B * v.y)));
		
		return;
	}
	
	if (abs(getDist(mp(x1, y1), mp(x2, y2)) + r1 - r2) < EPS)
	{
		pt v = mp(x1, y1) - mp(x2, y2);
		norm(v, r2);
		
		line cur = line(v.x, v.y, 0);
		
		v = v + mp(x2, y2);
		
		cur.C = -(cur.A * v.x + cur.B * v.y);
		
		tangs.pb(cur);
		
		return;
	}
	
	calc(mp(x1, y1), mp(x2, y2), r2 - r1);
	
	forn (i, sz(tangP))
	{
		line cur = line(mp(x1, y1), tangP[i]);

		pt v = tangP[i] - mp(x2, y2);
		norm(v, r1);

		cur.C -= cur.A * v.x + cur.B * v.y;
		
		tangs.pb(cur);
	}
}

inline void calcIn (ld x1, ld y1, ld x2, ld y2, ld r1, ld r2)
{
	if (abs(getDist(mp(x1, y1), mp(x2, y2)) - r1 - r2) < EPS)
	{
		pt v = mp(x1, y1) - mp(x2, y2);
		
		line cur = line(v.x, v.y, 0);
		
		norm(v, r2);
		v = v + mp(x2, y2);

		cur.C = -(cur.A * v.x + cur.B * v.y);
		
		tangs.pb(cur);
		
		return;
	}
	
	calc(mp(x1, y1), mp(x2, y2), r2 + r1);

	forn (i, sz(tangP))
	{
		line cur = line(mp(x1, y1), tangP[i]);

		pt v = mp(x2, y2) - tangP[i];
		norm(v, r1);

		cur.C -= cur.A * v.x + cur.B * v.y;
		
		tangs.pb(cur);
	}	
}

inline int calcTangs (ld x1, ld y1, ld r1, ld x2, ld y2, ld r2) //if r1 == 0 || r2 == 0 then undefine result, but may be it's wark true
{
	tangs.clear();

	if (r1 > r2 + EPS)
	{
		swap(r1, r2);
		swap(x1, x2);
		swap(y1, y2);
	}
	
	if (abs(x1 - x2) < EPS && abs(y1 - y2) < EPS)
	{
		if (abs(r1 - r2) < EPS)
			return -1;
		
		return 0;
	}
	
	calcOut(x1, y1, x2, y2, r1, r2);	
	calcIn(x1, y1, x2, y2, r1, r2);
	
	forn (i, sz(tangs))
		tangs[i].normal();
	
	sort(all(tangs));
	
	return sz(tangs);
}
