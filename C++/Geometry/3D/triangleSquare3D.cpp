inline ld det (ld a, ld b, ld c, ld d)
{
	return a * d - b * c;
}

inline ld getSq (const pt& a, const pt& b, const pt& c)
{
	ld ans = 0;

	ans += sqr(det(b.y - a.y, b.z - a.z, c.y - a.y, c.z - a.z));
	ans += sqr(det(b.z - a.z, b.x - a.x, c.z - a.z, c.x - a.x));
	ans += sqr(det(b.x - a.x, b.y - a.y, c.x - a.x, c.y - a.y));
	
	return sqrt(ans) / ld(2);
}