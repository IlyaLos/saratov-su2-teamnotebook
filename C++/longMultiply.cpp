
//modulo > 10^18
inline li myMul (li a, li b, li m)
{
 	li q = li(ld(a) * b / m);
 	li ans = a * b - q * m;

 	while (ans >= m)
 		ans -= m;

 	while (ans < 0)
 		ans += m;

 	return ans;
}

//modulo < 10^18
inline li myMul (li a, li b, li m)
{
 	li q = li(ld(a) * b / m);
 	li ans = (a * b - q * m) % m;

 	if (ans < 0)
 		ans += m;

 	return ans;
}
