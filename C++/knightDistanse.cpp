inline li getDist (const pt & a, const pt & b)
{
	li dx = abs(a.x - b.x);
	li dy = abs(a.y - b.y);

	if (dx == 1 && dy == 0)
		return 3;

	if (dx == 0 && dy == 1)
		return 3;

	if (dx == 2 && dy == 2)
		return 4;

	li lb = (dx + 1) / 2;
	lb = max(lb, (dy + 1) / 2);
	lb = max(lb, (dx + dy + 2) / 3);

	while ((lb & 1) != ((dx + dy) & 1))
		lb++;

	return lb;   
}
