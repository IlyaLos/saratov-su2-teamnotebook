const int N = 100 + 13;
int n, m; //count of vertexs and adges
vector<int> gg[N]; //original graph

const int M = 100 * 1000 + 13; //count of edges fo flow

struct edge
{
	int to;
	ld cap;
	ld flow;

	edge() {}
	edge(int to, ld cap) : to(to), cap(cap), flow(0) {}
};

int sze = 0;
edge e[M];

vector<int> g[N];

inline void addEdge(int v, int to, ld cap)
{
 	g[v].pb(sze);
 	e[sze++] = edge(to, cap);

 	g[to].pb(sze);
 	e[sze++] = edge(v, 0);
}

int q[M], head, tail;
char inQ[N];
ld d[N];
int p[N], pe[N];

ld flow;

inline bool enlarge(int s, int t)
{
	head = tail = 0;

	forn (i, N)
	{
		d[i] = 1e18;
		inQ[i] = 0;
	}

	d[s] = 0;
	inQ[s] = 1;
	q[tail++] = s;

	while (head != tail)
	{
		int v = q[head++];
		inQ[v] = 0;

		if (head == M)
			head = 0;

		forn (i, sz(g[v]))
		{
			int id = g[v][i];

			if (e[id].flow >= e[id].cap - EPS)
				continue;

			int to = e[id].to;

			if (d[to] > d[v] + 1 + EPS)
			{
				d[to] = d[v] + 1;
				p[to] = v;
				pe[to] = id;

				if (!inQ[to])
				{
					inQ[to] = 1;
					q[tail++] = to;

					if (tail == M)
						tail = 0;
				}
			}
		}
	}

	if (abs(d[t] - 1e18) < 1)
		return false;

	ld addFlow = 1e18;

	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];

		addFlow = min(addFlow, e[id].cap - e[id].flow);
	}

	flow += addFlow;

	for (int v = t; v != s; v = p[v])
	{
		int id = pe[v];

		e[id].flow += addFlow;
		e[id ^ 1].flow -= addFlow;
	}

	return true;
}

int s, t;

int szans;
int ans[N]; //answer subgraph

inline bool f (ld G)
{
	forn (i, n + 2)
		forn (j, sz(g[i]))
		{
			int id = g[i][j];

			e[id].flow = 0;

			if (e[id].to == t)
				e[id].cap = m + ld(2) * G - sz(gg[i]);
		}

	flow = 0;
	while (enlarge(s, t)) ;

	szans = 0;
	forn (i, n)
		if (abs(d[i] - 1e18) > 1)
			ans[szans++] = i;

	return szans != 0;
}

inline void findMaximalDensitySubgraph()
{
	if (m == 0)
	{
		printf ("1\n1\n");
		return;
	}

	s = n, t = n + 1;

	sze = 0;
	forn (i, n + 2)
		g[i].clear();

	forn (i, n)
	{
		forn (j, sz(gg[i]))
		{
			int to = gg[i][j];

			addEdge(i, to, ld(1));
		}

		addEdge(s, i, ld(m));
		addEdge(i, t, ld(0));
	}

	ld sh = ld(1) / (ld(n) * ld(n - 1)); //may be need more less
	ld l = 0, r = m;
	while (r - l >= sh - EPS)
	{
		ld mid = (l + r) / ld(2);

		if (f(mid))
			l = mid;
		else
			r = mid;
	}

	assert(f(l));

	printf ("%d\n", szans);
	forn (i, szans)
		printf ("%d\n", ans[i] + 1);
}
