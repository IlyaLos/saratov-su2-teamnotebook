const int N = 100 + 3;
int n;

vector<int> g[N];
const int M = 15;
int p[N];

int next[N], used[N], u = 0;

bool kuhn (int v)
{
	if (used[v] == u)
		return false;

	used[v] = u;

	forn (i, sz(g[v]))
	{
		int to = g[v][i];

		if (used[to] == u)
			continue;

		if (next[to] == -1 || kuhn(next[to]))
		{
			next[to] = v;
			next[v] = to;

			return true;
		}
	}

	return false;
}

int szans, sztmp;
pt ans[N], tmp[N];

inline void randomKuhn ()
{
	forn (i, n)
		p[i] = i;
		
	szans = 0;

	forn (it, M)
	{
		random_shuffle(p, p + n);

		forn (i, n)
			random_shuffle(all(g[i]));

		memset(next, -1, sizeof next);

		forn (i, n)
		{
			int v = p[i];

			if (next[v] != -1)
				continue;

			u++;

			kuhn(v);
		}

		sztmp = 0;

		forn (i, n)
		{
			int u = i;
			int v = next[i];

			if (u == -1)
				continue;

			if (u > v)
				continue;

			tmp[sztmp++] = mp(u, v);
		}
		
		if (sztmp > szans)
		{
			szans = sztmp;

			forn (i, szans)
				ans[i] = tmp[i];
		}
	}
}