const int N = 150 * 1000 + 13;
int n, m;
vector<int> g[N];
vector<pt> q[N];

int dsu[N];
int cl[N];
int anc[N];

int getParent (int v)
{
	if (dsu[v] == v)
		return v;

	return dsu[v] = getParent(dsu[v]);
}

inline void unite (int a, int b, int newAnc)
{
	a = getParent(a), b = getParent(b);

	if (a == b)
	{
		anc[a] = newAnc;
		return;
	}

	if (cl[a] < cl[b])
		swap(a, b);
	
	dsu[b] = a;
	cl[a]++;
	anc[a] = newAnc;
}

int used[N];
int ans[N];

void dfs (int v, int pr)
{
	used[v] = 1;

	forn (i, sz(g[v]))
	{
		int to = g[v][i];

		if (to == pr)
			continue;

		dfs(to, v);

		unite(v, to, v);
	}

	forn (i, sz(q[v]))
	{
		int to = q[v][i].ft;

		if (!used[to])
			continue;

		int id = q[v][i].sc;

		ans[id] = anc[ getParent(to) ];
	}
}

inline void solve()
{
	forn (i, n)
	{
		dsu[i] = i;
		cl[i] = 1;
		anc[i] = i;
	}

	dfs(0, -1);

	forn (i, m)
		printf ("%d ", ans[i] + 1);
	puts("");
}
