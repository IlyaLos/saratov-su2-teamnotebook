//это по рёбрам
//рёберная двусвязность - по вершинам (просто не ходим по мостам)

int used[N];
int tup[N], tin[N], T = 0;

stack<int> st;

int szans = 0;
vector<int> ans[N];

void dfs(int v, int pr = -1)
{
    used[v] = true;
    
    tin[v] = tup[v] = T++;
    
    forn(i, sz(g[v]))
    {
        int to = g[v][i].ft;
        int idx = g[v][i].sc;
        
        if (to == pr)
            continue;
            
        if (!used[to])
        {
            int sz = sz(st);
            
            st.push(idx);
            
            dfs(to, v);
            
            tup[v] = min(tup[v], tup[to]);

            if (tup[to] >= tin[v])
            {
                while(sz(st) != sz)
                {
                    int v = st.top();
                    st.pop();
                    
                    ans[szans].pb(v);
                }    
                
                szans++;
            }
        }
        else
        {
            if (tin[to] < tin[v])
                st.push(idx);
                
            tup[v] = min(tup[v], tin[to]);
        }
    }
    
    if (pr == -1 && sz(st) > 0)
    {
        while(sz(st) > 0)
        {
            int v = st.top();
            
            st.pop();
            
            ans[szans].pb(v);
        }
        
        szans++;
    }
}
