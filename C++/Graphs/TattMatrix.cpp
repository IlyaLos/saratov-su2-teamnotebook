//Для проверки на наличие совершенного парасочетания, требуется проверять, что определитель не равен нулю
const int K = 230;

vector<int> g[K];
ld d[K][K];
char used[K];

inline int calcRang(ld d[K][K], int n)
{
	int ans = n;
	forn(i, n)
		used[i] = 0;
		
    forn (i, n)
    {
    	int j = 0;
    	while(j < n)
    	{
    		if (!used[j] && abs(d[j][i]) > EPS)
    			break;

    		j++;
    	}
    
    	if (j == n)
    		ans--;
    	else 
    	{
    		used[j] = 1;
    		fore (p, i + 1, n - 1)
    			d[j][p] /= d[j][i];
    			
    		forn (k, n)
    			if (k != j && abs(d[k][i]) > EPS)
    				fore(p, i + 1, n - 1)
    					d[k][p] -= d[j][p] * d[k][i];
    	}
    }
    
    return ans;
}

inline int myRand()
{
	return rand() % 100;
}

const int M = 15;

inline int tatt ()
{
	int ans = 0;
	
	forn (it, M)
	{
     	forn (i, n * m)
     		forn (j, n * m)
     			d[i][j] = 0;

     	forn (i, n * m)
     		forn (j, sz(g[i]))
     		{
     			int to = g[i][j];
     			
     			if (i > to)
     				continue;
     				
     			int val = myRand() + 1;
     				
     			d[i][to] = val;
     			d[to][i] = -val;
     		}	
	
		ans = max(ans, calcRang(d, n * m));
	}
	
	return ans / 2;
}
