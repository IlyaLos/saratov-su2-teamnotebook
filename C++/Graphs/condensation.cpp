const int N = 100 * 1000 + 13;

vector<int> g[N], tg[N];
int used[N];
int p[N], szp;
int c[N], szc;
 
void dfs1 (int v) 
{
	used[v] = 1;

	forn (i, sz(g[v]))
	{
		int to = g[v][i];

		if (!used[to])
			dfs1(to);
	}

	p[szp++] = v;
}
 
void dfs2 (int v) 
{
	used[v] = 1;
	c[v] = szc;

	forn (i, sz(tg[v]))
	{
		int to = tg[v][i];

		if (!used[to])
			dfs2(to);
	}
}
 
inline void condensation () 
{
	memset(used, 0, sizeof used);
	szp = 0;

	forn (i, n)
		if (!used[i])
			dfs1(i);

	memset(used, 0, sizeof used);
	reverse(p, p + szp);

	forn (i, n)
	{
		int v = p[i];

		if (!used[v]) 
		{
			dfs2(v);
			szc++;
		}
	}
}