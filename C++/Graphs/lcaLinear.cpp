//Важно правильно подобрать константы. Эти верны для 10^5. K нужно брать чуть меньше, чем log2(2N)/2.
//Работает почти как двоичный подъём
//К этому алгоритму можно свести RMQ:
//Построим по массиву A декартово дерево, где у каждой вершины ключом будет позиция i, а приоритетом - само число A[i] (предполагается, что в декартовом дереве приоритеты упорядочены от меньшего в корне к большим). 
//Такое дерево можно построить за O (N). Тогда запрос RMQ(l,r) эквивалентен запросу LCA(l',r'), где l' - вершина, соответствующая элементу A[l], r' - соответствующая A[r]. 
//Действительно, LCA найдёт вершину, которая по ключу находится между l' и r', т.е. по позиции в массиве A будет между l и r, и при этом вершину, наиболее близкую к корню, 
//т.е. с наименьшим приоритетом, т.е. наименьшим значением.

const int N = 100 * 1000 + 13;
int h[N], order[2 * N], szo, first[N];

void dfs (int v, int pr, int deep = 0)
{
	h[v] = deep;
	order[szo++] = v;
	
	forn (i, sz(g[v]))
	{
		int to = g[v][i];
		
		if (to == pr)
			continue;
		
		dfs(to, v, deep + 1);
		
		order[szo++] = v;
	}
}

inline int minH (int a, int b)
{
	if (h[ order[a] ] <= h[ order[b] ])
		return a;
		
	return b;
}

const int K = 7; //log2(2 * N) / 2 - 2
const int M = 2 * N / (K - 1);
const int LOGM = 15; //log2(M)
const int SQRTN = 130; //2^K
int b[M];
int bst[LOGM][M];
int bh[M];
int prec[SQRTN][LOGM][LOGM];
int lg2[M];
int len, cntB;

inline void precalc ()
{
	szo = 0;
	dfs(0, 0);
	
	forn (i, szo)
		first[ order[i] ] = i;
	
	len = max(1, (int(log2(ld(szo))) + 1) / 2 - 2);
	cntB = (szo + len - 1) / len;
	
	memset(b, -1, sizeof b);
	
	forn (i, szo)
	{
		int id = i / len;
		
		if (b[id] == -1)
			b[id] = i;
		else
			b[id] = minH(b[id], i);
	}
	
	forn (i, cntB)
		bst[0][i] = b[i];
		
	for (int i = 1; (1 << i) <= cntB; i++)
	{
		assert(i < LOGM);

		for (int j = 0; j + (1 << i) <= cntB; j++)
			bst[i][j] = minH(bst[i - 1][j], bst[i - 1][j + (1 << (i - 1))]);
	}
			
	forn (i, cntB)
	{
		 int l = i * len, r = min(szo, (i + 1) * len) - 1;
		 
		 bh[i] = 0;
		 for (int j = l + 1; j <= r; j++)
		 	if (h[ order[j] ] - h[ order[j - 1] ] == -1)
		 		bh[i] = (bh[i] << 1) + 0;
		 	else
		 		bh[i] = (bh[i] << 1) + 1;
	}
	
	bh[cntB - 1] = (1 << len);
	
	memset(prec, -1, sizeof prec);
	
	forn (i, cntB)
	{
		int id = bh[i];
		
		if (prec[id][0][0] != -1)
			continue;
			
		for (int l = 0; l < len; l++)
		{
			prec[id][l][l] = l;
			
			for (int r = l + 1; r < len; r++)
			{
				prec[id][l][r] = prec[id][l][r - 1];
				
				if (i * len + r < szo)
					prec[id][l][r] = minH(i * len + prec[id][l][r], i * len + r) - i * len;
			}
		}
	}
	
	for (int i = 0, j = 0; i < M; i++)
	{
		if ((1 << (j + 1)) <= i)
			j++;
			
		lg2[i] = j;
	}
}

inline int inBl(int id, int l, int r)
{
	return prec[ bh[id] ][l][r] + id * len;
}

inline int lca (int a, int b)
{
	int l = first[a], r = first[b];
	if (l > r)
		swap(l, r);
		
	int lf = l / len, rg = r / len;
	int ll = l - lf * len, rr = r - rg * len;
	
	if (lf == rg)
		return order[ inBl(lf, ll, rr) ]; 
		
	int ans = minH(inBl(lf, ll, len - 1), inBl(rg, 0, rr));
	
	if (lf < rg - 1)
	{
		int pw2 = lg2[rg - lf - 1];
		ans = minH(ans, bst[pw2][lf + 1]);
		ans = minH(ans, bst[pw2][rg - (1 << pw2)]);
	}
	
	return order[ans];
}
