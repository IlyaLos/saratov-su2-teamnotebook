const int N = 100 + 13; //count of vertexes
int n, m; //count od vertexes and adges
vector<int> gg[N]; //original graph

const int M = 100 * 1000 + 13; //count of edges

struct edge
{
	int to;
	ld cap;
	ld flow;

	edge() {}
	edge(int to, ld cap) : to(to), cap(cap), flow(0) {}
};

int sze = 0;
edge e[M];

vector<int> g[N];

inline void addEdge(int v, int to, ld cap)
{
 	g[v].pb(sze);
 	e[sze++] = edge(to, cap);

 	g[to].pb(sze);
 	e[sze++] = edge(v, 0);
}

int d[N];
int ptr[N], q[M];
int s, t;

bool bfs()
{
	int head = 0, tail = 0;
	q[tail++] = s;

	forn (i, n + 2)
		d[i] = -1;

	d[s] = 0;

	while (head != tail && d[t] == -1)
	{
		int v = q[head++];
		if (head == M)
			head = 0;

		forn(i, sz(g[v]))
		{
			int id = g[v][i];
		    int to = e[id].to;

			if (d[to] == -1 && e[id].flow < e[id].cap - EPS)
			{
				q[tail++] = to;
				if (tail == M)
					tail = 0;

				d[to] = d[v] + 1;
			}
		}
	}

	return d[t] != -1;
}

ld dfs (int v, ld flow)
{
	if (abs(flow) < EPS)
	    return 0;

	if (v == t)
	    return flow;

	for (; ptr[v] < sz(g[v]); ++ptr[v])
	{
		int id = g[v][ptr[v]];
	    int to = e[id].to;

		if (d[to] != d[v] + 1)
		    continue;

		ld pushed = dfs(to, min(flow, e[id].cap - e[id].flow));

		if (pushed > EPS)
		{
			e[id].flow += pushed;
			e[id ^ 1].flow -= pushed;

			return pushed;
		}
	}

	return 0;
}

ld dinic()
{
	ld flow = 0;

	while(true)
	{
		if (!bfs())
		    break;

		forn (i, n + 2)
			ptr[i] = 0;

		ld pushed = dfs(s, 1e18);
		while (pushed > EPS)
		{
			flow += pushed;
			pushed = dfs(s, 1e18);
		}
	}

	return flow;
}

int used[N], u = 0;

void mark (int v)
{
	if (used[v] == u)
		return;

	used[v] = u;

	forn (i, sz(g[v]))
	{
		int id = g[v][i];

		if (e[id].flow >= e[id].cap - EPS)
			continue;

		int to = e[id].to;

		mark(to);
	}
}

int ans[N]; //answer subgraph
int szans;

inline bool f (ld G)
{
	forn (i, n + 2)
		forn (j, sz(g[i]))
		{
			int id = g[i][j];

			e[id].flow = 0;

			if (e[id].to == t)
				e[id].cap = m + ld(2) * G - sz(gg[i]);
		}

	dinic();

	u++;
	mark(s);

	szans = 0;
	forn (i, n)
		if (used[i] == u)
			ans[szans++] = i;

	return szans != 0;
}

inline void findMaximalDensitySubgraph()
{
	if (m == 0)
	{
		printf ("1\n1\n");
		return;
	}

	s = n, t = n + 1;

	sze = 0;
	forn (i, n + 2)
		g[i].clear();

	forn (i, n)
	{
		forn (j, sz(gg[i]))
		{
			int to = gg[i][j];

			addEdge(i, to, ld(1));
		}

		addEdge(s, i, ld(m));
		addEdge(i, t, ld(0));
	}

	ld sh = ld(1) / (ld(n) * ld(n - 1)); //may be more less
	ld l = 0, r = m;
	while (r - l >= sh - EPS)
	{
		ld mid = (l + r) / ld(2);

		if (f(mid))
			l = mid;
		else
			r = mid;
	}

	assert(f(l));

	printf ("%d\n", szans);
	forn (i, szans)
		printf ("%d\n", ans[i] + 1);
}
