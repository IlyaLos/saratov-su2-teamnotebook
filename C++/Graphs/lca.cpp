const int N = 100 * 1000 + 13;
const int LOGN = 20;

int n;
vector<int> g[N];

int tin[N], tout[N], Time = 0;
int a[N][LOGN];
int tmp[N], sztmp = 0;

void dfs (int v, int pr = -1)
{
	tin[v] = Time++;	
	tmp[sztmp++] = v;
		
	for (int i = 0; (1 << i) < sztmp; i++)
		a[v][i] = tmp[sztmp - 1 - (1 << i)];
		
	forn (i, sz(g[v]))
	{
		int to = g[v][i];
		
		if (to == pr)
			continue;
			
		dfs(to, v);
	}
	
	sztmp--;	
	tout[v] = Time++;
}

inline bool isParent (int a, int b)
{
	return tin[a] <= tin[b] && tout[a] >= tout[b];
}

inline int lca (int x, int y)
{
	if (isParent(x, y))
		return x;

	if (isParent(y, x))
		return y;

	for (int i = LOGN - 1; i >= 0; i--)
	{
		if (a[x][i] == -1)
			continue;
			
		if (!isParent(a[x][i], y))
			x = a[x][i];
	}
	
	return a[x][0];
}
