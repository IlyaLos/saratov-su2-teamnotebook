const int N = 310;

int n, m;
vector<int> g[N];

int match[N], p[N], blossom[N], base[N];

inline int lca(int a, int b)
{
	static int us[N];
	forn(i, n) us[i] = false;
	
	while (true)
	{
		a = base[a];
		us[a] = true;
		if (match[a] == -1) break;
		a = p[match[a]];
	}
	
	while (true)
	{
		b = base[b];
		if (us[b]) return b;
		b = p[match[b]];
	}
}

inline void markPath(int v, int b, int pr)
{
	while (base[v] != b)
	{
		blossom[base[v]] = blossom[base[match[v]]] = true;
		p[v] = pr;
		pr = match[v];
		v = p[match[v]];
	}
}

inline int augment(int root)
{
	static int used[N];
	forn(i, n) p[i] = -1, used[i] = false, base[i] = i;
	queue<int> q;
	used[root] = true;
	q.push(root);
	
	while (!q.empty())
	{
		int v = q.front();
		q.pop();
		
		forn(i, sz(g[v]))
		{
			int to = g[v][i];
			if (base[to] == base[v] || match[to] == v) continue;
			
			if (to == root || (match[to] != -1 && p[match[to]] != -1))
			{
				int b = lca(v, to);
				assert(base[b] == b);
				forn(j, n) blossom[j] = false;
				markPath(v, b, to);
				markPath(to, b, v);
				
				forn(j, n)
					if (blossom[base[j]])
					{
						base[j] = b;
						if (!used[j])
						{
    						used[j] = true;
    						q.push(j);
						}
					}
			}
			else if (p[to] == -1)
			{
				p[to] = v;
    			if (match[to] == -1) return to;
				used[match[to]] = true;
				q.push(match[to]);
			}
		}
	}
	return -1;
}

inline vector<pt> edmonds() // O(n^3)
{ // add greedy matching to make algo more fast
	forn(i, n) match[i] = -1;
	
	forn(i, n)
		if (match[i] == -1)
		{
			int to = augment(i);
			while (to != -1)
			{
				int next = match[p[to]];
				match[to] = p[to];
				match[p[to]] = to;
				to = next;
			}
		}
		
	vector<pt> ans;
	forn(i, n) if (match[i] != -1 && i < match[i]) ans.pb(mp(i, match[i]));
	return ans;
}
