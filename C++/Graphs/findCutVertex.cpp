int used[N];
int tin[N], tup[N], Time = 0;
 
void dfs (int v, int pr = -1) 
{
	used[v] = 1;
	tin[v] = tup[v] = Time++;
	
	int cl = 0;
	
	forn (i, sz(g[v]))
	{
		int to = g[v][i];
		if (to == pr)  
		    continue;
		    
		if (used[to])
		{
			tup[v] = min(tup[v], tin[to]);
		}
		else 
		{
			dfs (to, v);
			tup[v] = min(tup[v], tup[to]);
			
			if (pr != -1 && tup[to] >= tin[v])
				IS_CUT_VERTEX(v);
				
			cl++;
		}
	}
	
	if (pr == -1 && cl > 1)
	    IS_CUT_VERTEX(v);
}