//на примере задачи о покраске вершин дерева на расстоянии не более d в цвет c

int ids[N];

struct node
{
	int v;
	int pr;
	set<pair<int, pt> > q; //distance time color

	node() { v = -1, pr = -1, q.clear(); }
};

int szt;
node t[N]; //ровно n вершин

int used[N], u = 0;
int best;

int getCentr (int v, int pr, int allCnt)
{
	int cnt = 1;
	bool ok = 1;
	forn (i, sz(g[v]))
	{
		int to = g[v][i].ft;

		if (to == pr || used[to] != u)
			continue;

		int dto = getCentr(to, v, allCnt);

		if (dto > allCnt / 2)
			ok = 0;

		cnt += dto;
	}

	if (ok && allCnt - cnt <= allCnt / 2)
		best = v;

	return cnt;
}

vector<int> nv;

void calcNv (int v, int pr)
{
	nv.pb(v);

	forn (i, sz(g[v]))
	{
		int to = g[v][i].ft;

		if (to == pr || used[to] != u)
			continue;

		calcNv(to, v);
	}
}

void calc (const vector<int> v, int curT) //don't use &!
{
	if (sz(v) == 1)
	{
		t[curT].v = v[0];
		return;
	}

	u++;
	forn (i, sz(v))
		used[ v[i] ] = u;

	best = -1;
	getCentr(v[0], -1, sz(v));
	assert(best != -1);

	int curBest = best;

	t[curT].v = curBest;
	used[curBest] = 0;

	forn (i, sz(g[curBest]))
	{
		u++;
		forn (j, sz(v))
			used[ v[j] ] = u;
		used[curBest] = 0;

		int to = g[curBest][i].ft;

		if (used[to] != u)
			continue;

		t[szt].pr = curT;

		nv.clear();
		calcNv(to, curBest);

		calc(nv, szt++);
	}
}

// -------------LCA------------------
const int LOGN = 18;

int tin[N], tout[N], Time = 0;
int a[N][LOGN];
int tmp[N], sztmp = 0;
int h[N];

void dfs (int v, int pr = -1, int deep = 0)
{
	tin[v] = Time++;
	tmp[sztmp++] = v;
	h[v] = deep;

	for (int i = 0; (1 << i) < sztmp; i++)
		a[v][i] = tmp[sztmp - 1 - (1 << i)];

	forn (i, sz(g[v]))
	{
		int to = g[v][i].ft, len = g[v][i].sc;

		if (to == pr)
			continue;

		dfs(to, v, deep + len);
	}

	sztmp--;
	tout[v] = Time++;
}

inline bool isParent (int a, int b)
{
	return tin[a] <= tin[b] && tout[a] >= tout[b];
}

inline int lca (int x, int y)
{
	if (isParent(x, y))
		return x;

	if (isParent(y, x))
		return y;

	for (int i = LOGN - 1; i >= 0; i--)
	{
		if (a[x][i] == -1)
			continue;

		if (!isParent(a[x][i], y))
			x = a[x][i];
	}

	return a[x][0];
}

// -------------LCA------------------

inline int getDist (int a, int b)
{
	return h[a] + h[b] - 2 * h[lca(a, b)];
}

inline void prepare ()
{
	vector<int> v(n);
	forn (i, n)
		v[i] = i;

	szt = 1;
	calc(v, 0);

	forn (i, szt)
		ids[ t[i].v ] = i;

	dfs(0, -1);
}

inline void update (int v, int dist, int c, int time) //обновление вверх
{
	int s = v;
	while (v != -1)
	{
		int delDist = getDist(t[v].v, t[s].v);
		set<pair<int, pt> > &q = t[v].q;

		if (dist >= delDist)
		{
			while (!q.empty() && q.begin()->ft <= dist - delDist)
				q.erase(q.begin());

			q.insert(mp(dist - delDist, mp(time, c)));
		}

		v = t[v].pr;
	}
}

inline int getAns (int v) //проталкивание обновлений вниз
{
	int s = v;

	int ansTime = -1;
	int ansC = 0;

	while (v != -1)
	{
		int dist = getDist(t[v].v, t[s].v);

		set<pair<int, pt> > &q = t[v].q;
		set<pair<int, pt> >::iterator it = q.lower_bound(mp(dist, mp(-1, -1)));

		if (it != q.end())
		{
			int curTime = it->sc.ft;
			int curC = it->sc.sc;

			if (curTime > ansTime)
			{
				ansTime = curTime;
				ansC = curC;
			}
		}

		v = t[v].pr;
	}

	return ansC;
}