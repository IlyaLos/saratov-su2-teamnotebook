const int N = 1000 * 1000 + 13;
string s;
int a[N];
int n;

int pos[N], c[N];
int cnt[N], tmp[N];
int curLcp[N];

inline void sufmas() {
	s.pb(0);
	n++;

	memset(cnt, 0, sizeof(cnt));

	forn(i, n)
		cnt[ int(s[i]) ]++;
	fore(i, 1, N - 1)
		cnt[i] += cnt[i - 1];
	ford(i, n)
		pos[ --cnt[ int(s[i]) ] ] = i;

	int comp = 1;
	c[ pos[0] ] = comp - 1;

	fore(i, 1, n - 1) {
		if (s[ pos[i] ] != s[ pos[ i -1] ])
			comp++;
		c[ pos[i] ] = comp - 1;
	}

	for(int h = 0; (1 << h) < n && comp < n; h++) {
		forn(i, n)
			pos[i] = (pos[i] - (1 << h) + n) % n;
		forn(i, comp)
			cnt[i] = 0;
		forn(i, n)
			cnt[ c[i] ]++;
		fore(i, 1, comp - 1)
			cnt[i] += cnt[i - 1];
		ford(i, n)
			tmp[ --cnt[ c[ pos[i] ] ] ] = pos[i];
		forn(i, n)
			pos[i] = tmp[i];
		comp = 1;
		tmp[ pos[0] ] = comp - 1;
		fore(i, 1, n - 1) {
			int a = (pos[i] + (1 << h)) % n;
			int b = (pos[i - 1] + (1 << h)) % n;
			if (c[ pos[i] ] != c[ pos[i - 1] ] || c[a] != c[b])
					comp++;
			tmp[ pos[i] ] = comp - 1;
		}

		forn(i, n)
			c[i] = tmp[i];
	}

	forn(i, n - 1)
		a[i] = pos[i + 1];

	int last = 0;
	forn(i, n) {
		int a = i;
		if (c[a] == n - 1) {
			last = 0;
			continue;
		}
		int b = pos[ c[a] + 1];
		int val = max(0, last - 1);

		while (a + val < n && b + val < n && s[a + val] == s[b + val])
			val++;

		if (c[a] > 0)
			curLcp[ c[a] - 1] = val;
		last = val;
	}
	n--;
}
