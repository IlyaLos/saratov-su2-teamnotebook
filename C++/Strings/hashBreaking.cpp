#include<bits/stdc++.h>

using namespace std;

#define forn(i,n) for (int i = 0; i < int(n); i++)
#define ford(i,n) for (int i = int(n) - 1; i >= 0; i--)
#define fore(i,l,r) for (int i = int(l); i <= int(r); i++)
#define all(a) a.begin(), a.end()
#define sz(a) int(a.size())
#define mp make_pair
#define pb push_back
#define ft first
#define sc second
#define x first
#define y second

template<typename X> inline X abs(const X& a) { return a < 0 ? -a : a; }
template<typename X> inline X sqr(const X& a) { return a * a; }

typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;

const int INF = int(1e9);
const li INF64 = li(1e18);
const ld EPS = 1e-9;
const ld PI = acosl(ld(-1));

inline li myMul(li a, li b, li m)
{
	li q = ld(a) * b / m;
	li ans = (a * b - q * m) % m;
	while (ans < 0)
		ans += m;
	return ans;
}

struct tree
{
	li val;
	int id;
	tree *l, *r;

	tree(li val, int id, tree *l, tree *r ) : val(val), id(id), l(l), r(r) {}
};

typedef tree * ptree;

struct TreeComp
{
	bool operator () (const ptree& a, const ptree& b)
	{
		return a->val < b->val;
	}
};

const int N = 4000;

int an;
tree *a[N];
char s[N + 1], t[N + 1];

void result(tree *a, int f)
{
	if (!a->r)
	{
		int i = a->id;
		(f ? s : t)[N - i - 1]++;
		return;
	}

	int swap = (a->l->val < a->r->val);

	result(a->l, f ^ swap);
	result(a->r, f ^ swap ^ 1);
}

inline li getHash (li p, li q, char s[N + 1])
{
	li ans = 0;
	for (int i = 0; i < N; i++)
	{
		ans = myMul(ans, p, q);
		ans += s[i];
		if (ans >= q)
			ans -= q;
	}

	return ans;
}

int main()
{
	assert(freopen("input.txt", "r", stdin));
	assert(freopen("output.txt", "w", stdout));

	li p, q;
	cin >> p >> q;

	li f = 1;
	forn(i, N)
		a[an++] = new tree(f, i, 0, 0), f = myMul(f, p, q);

	while (an)
	{
		sort(a, a + an, TreeComp());

		if (a[0]->val == 0)
		{
			forn(i, N)
			s[i] = t[i] = 'a';
			result(a[0], 0);
			puts(s);
			puts(t);

			assert(getHash(p, q, s) == getHash(p, q, t));

			cerr << "OK" << endl;

			return 0;
		}

		forn(i, an / 2)
			a[i] = new tree(abs(a[2 * i]->val - a[2 * i + 1]->val), -1, a[2 * i], a[2 * i + 1]);

		an /= 2;
	}

	cerr << "not OK" << endl;
	puts("Impossible");

	return 0;
}