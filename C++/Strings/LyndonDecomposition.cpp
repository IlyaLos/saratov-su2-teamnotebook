//Строка называется простой, если строго меньше любого своего собственного 
//суффикса (то есть она меньше любого своего циклического сдвига)
//Декомпозиция Линдона - разложение строки s на строки простоые строки w1 >= w2 >= ... >= wk
//Такое разложение всегда существует и оно единственно

inline void lyndon (const string& s)
{
    int n = sz(s);
    int i = 0;
    while (i < n) 
    {
	    int j = i + 1, k = i;
	    while (j < n && s[k] <= s[j]) 
        {
	    	if (s[k] < s[j])
	    		k = i;
	    	else
	    		k++;
                
	    	j++;
	    }
        
	    while (i <= k) 
        {
    		cout << s.substr (i, j - k) << ' ';
    		i += j - k;
    	}
    }
}

//нахождение минимального циклического сдвига

inline string minCyclicShift (string s) //внутри происходит удвоение длины строки!
{
	s += s;
	int n = sz(s);
    
	int i = 0, ans = 0;
	while (i < n / 2) 
    {
		ans = i;
		int j = i + 1, k = i;
		while (j < n && s[k] <= s[j]) 
        {
			if (s[k] < s[j])
				k = i;
			else
				k++;
			
            j++;
		}
        
		while (i <= k)  
            i += j - k;
	}
    
	return s.substr (ans, n / 2);
}