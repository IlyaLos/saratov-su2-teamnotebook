const int N = 5 * 1000 * 1000 + 13;
char s[N];
int n;

struct node
{
	int len;
	int link;
	int next[2];
	
	node() {}
	node(int len) : len(len), link(-1) { memset(next, -1, sizeof next); }
};

int szt = 0;
node t[N];

inline void solve ()
{
	t[szt++] = node(-1);
	t[szt++] = node(0);
	t[szt - 1].link = 0;
	
	int curv = 0;
	forn (i, n)
	{
		char c = s[i];
		
		while (t[curv].len >= 0)
		{
			int pos = i - t[curv].len - 1;
			
			if (pos >= 0 && s[pos] == c)
				break;
				
			curv = t[curv].link;
		}
		
		if (t[curv].next[c - 'a'] == -1)
		{
			putchar('1');
			
			t[szt++] = node(t[curv].len + 2);
			
			int link = t[curv].link;
			
			while (link != -1)
			{
    			if (t[link].next[c - 'a'] != -1)
    			{
					int pos = i - t[link].len - 1;
			
    				if (s[pos] == c)
    				{			
    					t[szt - 1].link = t[link].next[c - 'a'];
    					break;
    				}
    			}
    			
    			link = t[link].link;
    		}
    		
    		if (t[szt - 1].len == 1)
    			t[szt - 1].link = 1;
	
			t[curv].next[c - 'a'] = szt - 1;
			
//			cerr << "ADD: " << szt - 1 << ' ' << curv << ' ' << t[szt - 1].len << ' ' << t[szt - 1].link << endl;
		}
		else
			putchar('0');
			
		curv = t[curv].next[c - 'a'];
	}
}
