const int N = 1000 * 1000 + 13;
string s;
int n;

struct node
{
	int l;
	int r; 
	int pr;
	map<char, int> next;
	int link;
	
	node() {}
	node(int L, int R, int Pr)
	{
	 	l = L, r = R, pr = Pr;
	 	link = -1;
	}
};

int szt = 0;
node t[2 * N + 1];

inline int leng (int v)
{
 	return t[v].r - t[v].l;
}

inline int add (int l, int r, int pr)
{
 	int nidx = szt++;
 	t[nidx] = node(l, r, pr);
 	t[pr].next[ s[l] ] = nidx;

 	return nidx;
}

inline int split (pt pos)
{
	int v = pos.ft, up = pos.sc, down = leng(v) - up;

	if (up == 0)
		return v;

	if (down == 0)
		return t[v].pr;

	int mid = add(t[v].l, t[v].l + down, t[v].pr);
	t[v].l += down;
	t[v].pr = mid;
	t[mid].next[ s[ t[v].l ] ] = v;

	return mid;
}

inline pt go (pt pos, char c)
{
 	int v = pos.ft, up = pos.sc;
 	if (up > 0)
 		return s[ t[v].r - up ] == c ? mp(v, up - 1) : mp(-1, -1);
 	 
   	int nextv = t[v].next.count(c) ? t[v].next[c] : -1;
   	return nextv != -1 ? mp(nextv, leng(nextv) - 1) : mp(-1, -1);
}

inline pt fastGo (int v, int l, int r)
{
 	if (l == r)
 		return mp(v, 0);

	while (true)
	{
	 	v = t[v].next[ s[l] ]
	 	if (leng(v) >= r - l)
	 		return mp(v, leng(v) - r + l);

	 	l += leng(v);
	}

	throw;
}

int link (int v)
{
 	if (t[v].link == -1) 
 		t[v].link = split(fastGo(link(t[v].pr), t[v].l + int(t[v].pr == 0), t[v].r));

 	return t[v].link;
}

inline pt movei (pt ptrj, int i)
{
 	while (true)
 	{
 	 	pt nptrj = go(ptrj, s[i]);

 	 	if (nptrj.ft != -1)
 	 		return nptrj;
 	 	else
 	 	{
 	 	 	int mid = split(ptrj);
 	 	 	add(i, n, mid);
 	 	 	ptrj = mp(link(mid), 0);

 	 	 	if (mid == 0)
 	 	 		return ptrj;
 	 	}
 	}

 	throw;
}

inline void make_tree ()
{
 	szt = 1;
 	t[0] = node(-1, -1, 0);
 	t[0].link = 0;

 	pt ptrj = mp(0, 0);

 	forn (i, n)
 		ptrj = movei(ptrj, i);
}

inline void solve () 
{	
	s.pb(0);
	n++;

	make_tree();
	
	n--;
}
