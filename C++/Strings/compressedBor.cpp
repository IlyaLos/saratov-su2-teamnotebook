const int N = 1000 * 1000 + 13;

string S;

struct vert
{
	map<char, int> next;
	
	int lf, rg;

	int cnt;
	int term;
};

vert t[N];
int szt;

int n;
char buf[N];

inline int addVert ()
{
	t[szt].next.clear();
	t[szt].lf = 0;
	t[szt].rg = -1;

	t[szt].cnt = 0;
	t[szt].term = -1;
	
	return szt++;
}

inline void init ()
{
	szt = 0;

	addVert();			
}

inline void addString (int lf, int rg, int idx)
{
	int v = 0;
	
	for (int i = lf; i <= rg; i++)
	{
		char c = S[i];
		
		if (!t[v].next.count(c))
		{
			int to = addVert();
	
			t[v].next[c] = to;
			t[to].lf = i, t[to].rg = rg;
			
			v = to;
			
			t[v].cnt++;
			
			break;
		}	

		int to = t[v].next[c];
		int curLf = t[to].lf, curRg = t[to].rg;
		
		int j = i, it = curLf;
		while (j <= rg && it <= curRg)
		{
			if (S[j] != S[it])
				break;
				
			j++, it++;
	 	}
	 	
	 	if (it == curRg + 1)
	 	{
	 		i = j - 1;
	 		
	 		v = to;

	 		t[v].cnt++;	
	 	}
	 	else
	 	{
	 		int tmpV = addVert();
	 		
	 		t[v].next[c] = tmpV;
	 		t[tmpV].lf = curLf, t[tmpV].rg = it - 1;

	 		t[tmpV].next[ S[it] ] = to;
	 		t[to].lf = it, t[to].rg = curRg;

	 		t[tmpV].cnt = t[to].cnt + 1;	 		
	 		
	 		v = tmpV;
	 		i = j - 1;	 		
	 	}	
	}

	t[v].term = idx;
}

inline bool read ()
{
	if (scanf ("%d", &n) != 1)
		return false;
		
	init();
	
	forn (i, n)
	{
		assert(scanf ("%s", buf) == 1);	
		
		S += string(buf);
		
		addString(sz(S) - strlen(buf), sz(S) - 1, i + 1);
	}
	
	return true;
}
