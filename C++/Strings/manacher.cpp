// sh = 0 - odd, 1 - even
// aaaa -> sh=0 [1, 2, 2, 1] sh=1 [0, 1, 2, 1]
inline void manacher (char b[N], int n, int d[N], int sh) //string, len, ans, sh = {0, 1}
{
	int g = 0;
	d[0] = 1 - sh;

	fore(i, 1, n)
	{
		d[i] = 0;
		if (2 * g - i >= 0)
			d[i] = max(min(d[2 * g - i], d[g] + g - i), 0);

		while(i >= d[i] + sh && i + d[i] < n && b[ i + d[i] ] == b[ i - d[i] - sh ])
		{
			g = i;
			d[g]++;
		}
	}
}