	for(int mask = (1 << k) - 1; mask < (1 << n); ) {
		if (mask == 0) break;
		int x = mask & -mask, y = mask + x;
		mask = (( (mask & ~y) / x ) >> 1) | y;
	}
