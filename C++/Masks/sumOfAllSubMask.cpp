for(int i = 0; i < n; i++) {
  for(int mask = 0; mask < (1 << n); mask++) {
     if (hasBit(mask, i)) {
         d[mask] += d[mask ^ (1 << i)];
     }
  }
}