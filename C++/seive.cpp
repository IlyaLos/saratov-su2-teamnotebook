const int N = 100000000;

int lp[N]; //smallest prime which divides number i
int primes[N], szpr; 

void get_seiveFast()  // calculation of seive
{
 	for (int i = 2; i < N; ++i) 
 	{
		if (lp[i] == 0) 
		{
			lp[i] = i;
			primes[szpr++] = i;
		}

		for (int j = 0; j < szpr && primes[j] <= lp[i] && i * primes[j] < N; ++j)
			lp[i * primes[j]] = primes[j];
	}
}
